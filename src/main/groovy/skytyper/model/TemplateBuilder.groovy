/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model

import com.fasterxml.jackson.annotation.JsonIgnore

class TemplateBuilder extends Template {

    @JsonIgnore
    ResourceBuilder resource
    @JsonIgnore
    ParameterBuilder parameter

    TemplateBuilder() {
        resource = new ResourceBuilder(this)
        parameter = new ParameterBuilder(this)
    }

    void output(String name, @DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = Output) Closure closure) {
        def output = new Output()
        output.name = name
        closure.delegate = output
        closure()
        add output
    }

    void mapping(Map m) { add m }

}

