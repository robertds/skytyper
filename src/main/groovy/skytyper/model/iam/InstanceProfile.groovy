/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.iam

import skytyper.model.Resource
import org.hibernate.validator.constraints.NotEmpty

import javax.validation.constraints.NotNull

class InstanceProfile extends Resource {
    static final String TYPE = "AWS::IAM::InstanceProfile"
    @NotEmpty
    String name = ''

    @NotEmpty
    String path
    @NotNull
    def roles

    def data() {
        def properties = [:]
        if (path) properties << ["Path": path ]
        if (roles) properties << ["Roles": roles.collect{ it } ]
        [ "Type": TYPE, "Properties": properties ] }

    String getType() { TYPE }
}
