/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.iam

import skytyper.model.Resource
import org.hibernate.validator.constraints.NotEmpty
import groovy.json.*

class Role extends Resource {
    static final String TYPE = "AWS::IAM::Role"
    @NotEmpty
    String name = ''
    @NotEmpty
    String assumeRolePolicyDocument = '{ "Statement": [ {\n' +
            '                  "Effect": "Allow",\n' +
            '                  "Principal": {\n' +
            '                     "Service": [ "ec2.amazonaws.com" ]\n' +
            '                  },\n' +
            '                  "Action": [ "sts:AssumeRole" ]\n' +
            '               } ] }'
    @NotEmpty
    String path
    Policy[] policies

    def data() {
        def slurper = new JsonSlurper()

        def properties = [:]
        if (assumeRolePolicyDocument) properties << ["AssumeRolePolicyDocument": slurper.parseText(assumeRolePolicyDocument) ]
        if (path) properties << ["Path": path ]
        if (policies) properties << ["Policies": policies.collect{ it.data() } ]
        [ "Type": TYPE, "Properties": properties ] }

    String getType() { TYPE }
}
