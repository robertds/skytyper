/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model

import skytyper.model.autoscaling.AutoScalingResourceBuilder
import skytyper.model.ec2.EC2ResourceBuilder


class ResourceBuilder {

    private Template template

    public EC2ResourceBuilder ec2
    public AutoScalingResourceBuilder autoscaling

    ResourceBuilder(Template template) {
        this.template = template
        ec2 = new EC2ResourceBuilder(template)
        autoscaling = new AutoScalingResourceBuilder(template)

    }

}