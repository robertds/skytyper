/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.autoscaling

import skytyper.model.Resource
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.annotation.JsonValue
import org.hibernate.validator.constraints.NotEmpty

import javax.validation.Valid
import javax.validation.constraints.NotNull

//TODO Add builder methods for ScalingPolicyProperties embedded types
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder([ "Type" ])
class ScalingPolicy extends Resource {
    static final String TYPE = "AWS::AutoScaling::ScalingPolicy"
    @JsonIgnore
    String getType() { TYPE }

    @NotEmpty
    @JsonIgnore
    String name = ''

    @NotNull
    @JsonProperty("Properties")
    @Valid
    ScalingPolicyProperties properties

    ScalingPolicy() {
        properties = new ScalingPolicyProperties();
    }

    ScalingPolicy(String name) {
        this()
        this.name = name
    }

    def properties(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ScalingPolicyProperties) Closure closure) {
        def props = new ScalingPolicyProperties()
        def clone = closure.rehydrate(props, this, this)
        clone()
        this.properties = props
        props
    }
}

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class ScalingPolicyProperties {
    @JsonProperty('AdjustmentType')
    @NotNull
    ScalingPolicyAdjustmentType adjustmentType

    @NotNull
    @JsonProperty('AutoScalingGroupName')
    String autoScalingGroupName

    @JsonProperty('Cooldown')
    Integer cooldown

    @NotNull
    @JsonProperty('ScalingAdjustment')
    Integer scalingAdjustment
}

enum ScalingPolicyAdjustmentType {
    CHANGE_IN_CAPACITY('ChangeInCapacity'),
    EXACT_CAPACITY('ExactCapacity'),
    PERCENT_CHANGE_IN_CAPACITY('PercentChangeInCapacity')

    final String name

    ScalingPolicyAdjustmentType(String name) {
        this.name = name
    }

    @JsonValue
    String value() {
        name
    }

    @Override
    String toString() {
        value()
    }

    @JsonCreator
    static ScalingPolicyAdjustmentType fromValue(String typeString) {
        for (ScalingPolicyAdjustmentType t: values()) {
            if (t.value().toLowerCase() == typeString.toLowerCase()) {
                return t;
            }
        }
        throw new IllegalArgumentException("Invalid ScalingPolicyAdjustmentType value: "+typeString)
    }
}
