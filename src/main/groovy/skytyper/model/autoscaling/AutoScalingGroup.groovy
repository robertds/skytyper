/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.autoscaling

import skytyper.model.Parameter
import skytyper.model.Resource
import skytyper.model.TemplateFunctions
import skytyper.validation.ValidAutoScalingGroup
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import org.hibernate.validator.constraints.NotEmpty
import javax.validation.Valid
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder([ "Type" ])
@ValidAutoScalingGroup
class AutoScalingGroup extends Resource {
    static final String TYPE = "AWS::AutoScaling::AutoScalingGroup"

    @JsonIgnore
    String getType() { TYPE }

    @NotEmpty
    @JsonIgnore
    String name = ''

    @NotNull
    @Valid
    @JsonProperty('Properties')
    AutoScalingGroupProperties properties

    @Valid
    @JsonProperty('UpdatePolicy')
    UpdatePolicyAttribute updatePolicy

    AutoScalingGroup() {
        properties = new AutoScalingGroupProperties();
    }

    AutoScalingGroup(String name) {
        this()
        this.name = name
    }

    def properties(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = AutoScalingGroupProperties) Closure closure) {
        def props = new AutoScalingGroupProperties()
        def clone = closure.rehydrate(props, this, this)
        clone()
        this.properties = props
        props
    }

    def updatePolicy(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = UpdatePolicyAttribute) Closure closure) {
        def policy = new UpdatePolicyAttribute()
        def clone = closure.rehydrate(policy, this, this)
        clone()
        this.updatePolicy = policy
        policy
    }


}

class CFnInteger extends TemplateFunctions {
    def _value

    CFnInteger(int i) {
        _value = new Integer(i)
    }

    CFnInteger(Integer i) {
        _value = i
    }

    CFnInteger(Parameter p) {
        _value = ref(p)
    }
}

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class AutoScalingGroupProperties extends TemplateFunctions {
    @NotEmpty
    @JsonProperty('AvailabilityZones')
    String[] availabilityZones = []

    @JsonProperty('Cooldown')
    def cooldown

    @JsonProperty('DesiredCapacity')
    String desiredCapacity

    @JsonProperty('HealthCheckGracePeriod')
    Integer healthCheckGracePeriod

    @JsonProperty('HealthCheckType')
    HealthCheckType healthCheckType

    @JsonProperty('InstanceId')
    String instanceID

    @JsonProperty('LaunchConfigurationName')
    String launchConfigurationName

    @JsonProperty('LoadBalancerNames')
    String[] loadBalancerNames

//    @Min(value = 1L)
    @NotNull
    @JsonProperty('MaxSize')
    def maxSize

    void setMaxSize(int i) {
        maxSize = new Integer(i)
    }

    void setMaxSize(Resource r) {
        maxSize = ref(r)
    }

//    @Min(value = 1L)
    @NotNull
    @JsonProperty('MinSize')
    def minSize

    @Valid
    @JsonProperty('NotificationConfiguration')
    NotificationConfigurationProperty notificationConfiguration

    @Valid
    @JsonProperty('Tags')
    AutoScalingTag[] tags

    @JsonProperty('VPCZoneIdentifier')
    String[] vpcZoneIdentifier
}


@JsonInclude(JsonInclude.Include.NON_EMPTY)
class NotificationConfigurationProperty {
    @NotEmpty
    @JsonProperty('TopicARN')
    String topicARN

    @NotEmpty
    @JsonProperty('NotificationTypes')
    NotificationType[] notificationTypes
}

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class UpdatePolicyAttribute {
    @NotNull
    @Valid
    @JsonProperty('AutoScalingRollingUpdate')
    AutoScalingRollingUpdate autoScalingRollingUpdate

    UpdatePolicyAttribute() {
        autoScalingRollingUpdate = new AutoScalingRollingUpdate()
    }

    def autoScalingRollingUpdate(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = AutoScalingRollingUpdate) Closure closure) {
        def update = new AutoScalingRollingUpdate()
        def clone = closure.rehydrate(update, this, this)
        clone()
        this.autoScalingRollingUpdate = update
        update
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    class AutoScalingRollingUpdate {
        @JsonProperty('MaxBatchSize')
        Integer maxBatchSize

        @JsonProperty('MinInstancesInService')
        Integer minInstancesInService

        @Pattern(regexp=/^PT(\d+H)?(\d+M)?(\d+S)?$/)
        @JsonProperty('PauseTime')
        String pauseTime
    }

}


enum NotificationType {
    LAUNCH('autoscaling:EC2_INSTANCE_LAUNCH'),
    LAUNCH_ERROR('autoscaling:EC2_INSTANCE_LAUNCH_ERROR'),
    TERMINATE('autoscaling:EC2_INSTANCE_TERMINATE'),
    TERMINATE_ERROR('autoscaling:EC2_INSTANCE_TERMINATE_ERROR'),
    TEST('autoscaling:TEST_NOTIFICATION')
    String value

    NotificationType(String value) {
        this.value = value
    }

    static NotificationType[] allTypes() {
        return [ LAUNCH, LAUNCH_ERROR, TERMINATE, TERMINATE_ERROR, TEST]
    }
}

enum HealthCheckType {
    EC2, ELB
}

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class AutoScalingTag {
    @NotEmpty
    @JsonProperty('Key')
    String key

    @NotEmpty
    @JsonProperty('Value')
    String value

    @JsonProperty('PropagateAtLaunch')
    boolean propagateAtLaunch = false
}

