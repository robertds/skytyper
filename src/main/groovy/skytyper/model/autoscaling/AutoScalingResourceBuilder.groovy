/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.autoscaling

import skytyper.model.Template
import skytyper.model.TemplateFunctions

class AutoScalingResourceBuilder extends TemplateFunctions {
    private Template template

    AutoScalingResourceBuilder(Template template) {
        this.template = template
    }

    def autoScalingGroup(String name, @DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = AutoScalingGroup) Closure closure) {
        def resource = new AutoScalingGroup()
        resource.name = name
        closure.delegate = resource
        closure()
        template.add resource
        resource
    }

    def launchConfiguration(String name, @DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = LaunchConfiguration) Closure closure) {
        def resource = new LaunchConfiguration()
        resource.name = name
        closure.delegate = resource
        closure()
        template.add resource
        resource
    }

    def scalingPolicy(String name, @DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ScalingPolicy) Closure closure) {
        def resource = new ScalingPolicy()
        resource.name = name
        closure.delegate = resource
        closure()
        template.add resource
        resource
    }

    def scheduledAction(String name, @DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ScheduledAction) Closure closure) {
        def resource = new ScheduledAction()
        resource.name = name
        closure.delegate = resource
        closure()
        template.add resource
        resource
    }

}
