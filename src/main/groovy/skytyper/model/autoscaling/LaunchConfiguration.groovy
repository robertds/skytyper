/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.autoscaling

import skytyper.model.Resource
import skytyper.model.ec2.EC2SecurityGroup
import skytyper.model.ec2.InstanceType
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.annotation.JsonValue
import org.hibernate.validator.constraints.NotEmpty

import javax.validation.Valid
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

//TODO Add builder methods for LaunchConfigurationProperties embedded types
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder([ "Type" ])
class LaunchConfiguration extends Resource {
    static final String TYPE = "AWS::AutoScaling::LaunchConfiguration"
    @JsonIgnore
    String getType() { TYPE }

    @NotEmpty
    @JsonIgnore
    String name = ''

    @NotNull
    @JsonProperty("Properties")
    @Valid
    LaunchConfigurationProperties properties

    LaunchConfiguration() {
        properties = new LaunchConfigurationProperties();
    }

    LaunchConfiguration(String name) {
        this()
        this.name = name
    }

    def properties(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = LaunchConfigurationProperties) Closure closure) {
        def props = new LaunchConfigurationProperties()
        def clone = closure.rehydrate(props, this, this)
        clone()
        this.properties = props
        props
    }
}

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class LaunchConfigurationProperties {
    @JsonProperty('AssociatePublicIpAddress')
    Boolean associatePublicIPAddress

    @JsonProperty('BlockDeviceMappings')
    AutoScalingBlockDeviceMappingProperty[] blockDeviceMappings = []

    @JsonProperty('EbsOptimized')
    Boolean ebsOptimized

    @Size(min=1, max=1600)
    @JsonProperty('IamInstanceProfile')
    String iamInstanceProfile

    @NotNull
    @JsonProperty('ImageId')
    String imageID

    @JsonProperty('InstanceID')
    String instanceID

    @JsonProperty('InstanceMonitoring')
    Boolean instanceMonitoring

    @NotNull
    @JsonProperty('InstanceType')
    InstanceType instanceType

    @JsonProperty('KernelId')
    String kernelID

    @JsonProperty('KeyName')
    String keyName

    @JsonProperty('RamDiskId')
    String ramDiskID

    @JsonProperty('SecurityGroups')
    EC2SecurityGroup[] securityGroups

    @JsonProperty('SpotPrice')
    String spotPrice

    @JsonProperty('UserData')
    String userData
}

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class AutoScalingBlockDeviceMappingProperty {
    //TODO Add class validator for AutoScalingBlockDeviceMappingProperty
    @NotEmpty
    @JsonProperty("DeviceName")
    String deviceName

    @JsonProperty("Ebs")
    AutoScalingEBSBlockDeviceProperty ebsBlockDevice

    @JsonProperty("NoDevice")
    Boolean noDevice

    @Pattern(regexp = /^ephemeral\d+$/)
    @JsonProperty("VirtualName")
    String virtualName
}

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class AutoScalingEBSBlockDeviceProperty {
    //TODO Add class validator for AutoScalingEBSBlockDeviceProperty
    @JsonProperty("DeleteOnTermination")
    Boolean deleteOnTermination

    @JsonProperty("Iops")
    Integer iops

    @JsonProperty("SnapshotId")
    String snapshotID

    @Min(value = 1L)
    @JsonProperty("VolumeSize")
    Integer volumeSize

    @JsonProperty("VolumeType")
    AutoScalingEBSBlockDeviceVolumeType volumeType

    enum AutoScalingEBSBlockDeviceVolumeType {
        STANDARD('standard'),
        IO1('io1')
        String value

        AutoScalingEBSBlockDeviceVolumeType(String value) {
            this.value = value
        }

        @JsonValue
        String value() {
            value
        }

        @Override
        String toString() {
            value()
        }

        @JsonCreator
        static AutoScalingEBSBlockDeviceVolumeType fromValue(String typeString) {
            for (AutoScalingEBSBlockDeviceVolumeType t: values()) {
                if (t.value() == typeString.toLowerCase()) {
                    return t;
                }
            }
            throw new IllegalArgumentException("Invalid AutoScalingEBSBlockDeviceVolumeType value: "+typeString)
        }
    }

}
