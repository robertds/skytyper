/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import org.hibernate.validator.constraints.NotEmpty

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "Type",
        visible = false
)
@JsonSubTypes( [
        @JsonSubTypes.Type(value = StringParameter.class, name = "String"),
        @JsonSubTypes.Type(value = NumberParameter.class, name = "Number") ]
)
abstract class Parameter {
    @NotEmpty
    @JsonIgnore
    String name = ''

    @JsonProperty("Description")
    String description = ''

    @JsonProperty("Default")
    def defaultValue

    @JsonProperty("NoEcho")
    def noEcho

    @JsonIgnore
    boolean isNoEcho() { if (noEcho instanceof Boolean) return noEcho}

    @JsonProperty("AllowedValues")
    String[] allowedValues = []

    @JsonProperty("ConstraintDescription")
    String constraintDescription = ''

    abstract String getType()
}
