/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model

import skytyper.model.autoscaling.*
import skytyper.model.ec2.*
import skytyper.model.elasticloadbalancing.LoadBalancer
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonValue

//FIXME Add all resource types to @JsonSubTypes
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "Type",
        visible = false
)
@JsonSubTypes( [
    @JsonSubTypes.Type(value = EC2Instance.class, name = EC2Instance.TYPE),
    @JsonSubTypes.Type(value = EC2SecurityGroup.class, name = EC2SecurityGroup.TYPE),
    @JsonSubTypes.Type(value = EIP.class, name = EIP.TYPE),
    @JsonSubTypes.Type(value = Route.class, name = Route.TYPE),
    @JsonSubTypes.Type(value = AutoScalingGroup.class, name = AutoScalingGroup.TYPE),
    @JsonSubTypes.Type(value = LaunchConfiguration.class, name = LaunchConfiguration.TYPE),
    @JsonSubTypes.Type(value = ScalingPolicy.class, name = ScalingPolicy.TYPE),
    @JsonSubTypes.Type(value = ScheduledAction.class, name = ScheduledAction.TYPE),
    @JsonSubTypes.Type(value = LoadBalancer.class, name = LoadBalancer.TYPE),
] )
abstract class Resource extends TemplateFunctions {
    //TODO Add resource name validation to ensure no spaces or non-alpha characters
    String name = ''

    abstract String getType()

    @JsonProperty("Metadata")
    def metadata

    @JsonProperty("DependsOn")
    def dependsOn

    @JsonProperty("DeletionPolicy")
    def deletionPolicy

}

enum DeletionPolicy {
    //TODO Add DeletionPolicy unit tests
    DELETE("Delete"),
    RETAIN("Retain"),
    SNAPSHOT("Snapshot")
    private String value

    def DeletionPolicy(String s) {
        value = s
    }

    @JsonValue
    String value() {
        value
    }

    @JsonCreator
    static DeletionPolicy fromValue(String typeString) {
        for (DeletionPolicy t: values()) {
            if (t.value() == typeString.toLowerCase()) {
                return t;
            }
        }
        throw new IllegalArgumentException("Invalid DeletionPolicy value: "+typeString)
    }
}
