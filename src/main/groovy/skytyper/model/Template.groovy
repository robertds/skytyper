/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.databind.ObjectMapper

import javax.validation.Valid
import javax.validation.constraints.Size

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder([ "AWSTemplateFormatVersion", "Description", "Parameters", "Mappings", "Resources", "Outputs" ])
class Template extends TemplateFunctions {
    @JsonProperty("AWSTemplateFormatVersion")
    private final String TEMPLATE_VERSION = '2010-09-09'
    @JsonIgnore
    private HashMap<String, String> buildProperties = [:]

    @JsonProperty("Description")
    // Note: This property was named templateDescription to disambiguate it from other resource descriptions
    //       as well as to avoid conflicts within resource closures.
    String templateDescription = ''

    @Valid
    @Size(max = 50)
    @JsonProperty("Parameters")
    public LinkedHashMap<String, Parameter> parameters = [:]

    @JsonProperty("Mappings")
    public def mappings = [:]

    @Valid
    @Size(max = 50)
    @JsonProperty("Conditions")
    public LinkedHashMap<String, Condition> conditions = [:]

    @Valid
    @JsonProperty("Resources")
    public LinkedHashMap<String, Resource> resources = [:]

    @Valid
    @Size(max = 10)
    @JsonProperty("Outputs")
    public LinkedHashMap<String, Output> outputs = [:]

    // Constructors, allow for build properties upon creation

    Template() {}

    Template(HashMap<String, String> _buildProperties) {
        buildProperties = _buildProperties
    }


    // Convenience methods to add to template maps

    void add(Parameter p) { parameters << [ (p.name) : p ] }

    void add(Map m) { mappings << m }

    void add(Condition c) { conditions << [ (c.name) : c.value ] }

    void add(Resource r) { resources << [ (r.name) : r ] }

    void add(Output o) { outputs << [ (o.name) : o ] }


    @Override
    String toString() {
        new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this)
    }

}
