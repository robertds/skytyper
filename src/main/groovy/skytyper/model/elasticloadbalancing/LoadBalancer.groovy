/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.elasticloadbalancing

import skytyper.model.Resource
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import org.hibernate.validator.constraints.NotEmpty

import javax.validation.Valid
import javax.validation.constraints.NotNull

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder([ "Type" ])
class LoadBalancer extends Resource {
    static final String TYPE = "AWS::ElasticLoadBalancing::LoadBalancer"
    @JsonIgnore
    String getType() { TYPE }

    @NotEmpty
    @JsonIgnore
    String name = ''

    @NotNull
    @JsonProperty("Properties")
    @Valid
    LoadBalancerProperties properties

    LoadBalancer() {
        properties = new LoadBalancerProperties();
    }

    LoadBalancer(String name) {
        this()
        this.name = name
    }

    def properties(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = LoadBalancerProperties) Closure closure) {
        def props = new LoadBalancerProperties()
        def clone = closure.rehydrate(props, this, this)
        clone()
        this.properties = props
        props
    }
}

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class LoadBalancerProperties {
    //FIXME Incomplete implmentation

    @NotNull
    @JsonProperty('AccessLoggingPolicy')
    String autoScalingGroupName

    @JsonProperty('DesiredCapacity')
    Integer desiredCapacity

    //TODO Change type to DateTime
    @JsonProperty('EndTime')
    String endTime

    @JsonProperty('MaxSize')
    Integer maxSize

    @JsonProperty('MinSize')
    Integer minSize


    //TODO Add cron regex: (((([0-9]|[0-5][0-9]),)*([0-9]|[0-5][0-9]))|(([0-9]|[0-5][0-9])(/|-)([0-9]|[0-5][0-9]))|([\?])|([\*]))[\s](((([0-9]|[0-5][0-9]),)*([0-9]|[0-5][0-9]))|(([0-9]|[0-5][0-9])(/|-)([0-9]|[0-5][0-9]))|([\?])|([\*]))[\s](((([0-9]|[0-1][0-9]|[2][0-3]),)*([0-9]|[0-1][0-9]|[2][0-3]))|(([0-9]|[0-1][0-9]|[2][0-3])(/|-)([0-9]|[0-1][0-9]|[2][0-3]))|([\?])|([\*]))[\s](((([1-9]|[0][1-9]|[1-2][0-9]|[3][0-1]),)*([1-9]|[0][1-9]|[1-2][0-9]|[3][0-1])(C)?)|(([1-9]|[0][1-9]|[1-2][0-9]|[3][0-1])(/|-)([1-9]|[0][1-9]|[1-2][0-9]|[3][0-1])(C)?)|(L)|(LW)|([1-9]W)|([1-3][0-9]W)|([\?])|([\*]))[\s](((([1-9]|0[1-9]|1[0-2]),)*([1-9]|0[1-9]|1[0-2]))|(([1-9]|0[1-9]|1[0-2])(/|-)([1-9]|0[1-9]|1[0-2]))|(((JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC),)*(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))|((JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)(-|/)(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))|([\?])|([\*]))[\s]((([1-7],)*([1-7]))|([1-7](/|-)([1-7]))|(((MON|TUE|WED|THU|FRI|SAT|SUN),)*(MON|TUE|WED|THU|FRI|SAT|SUN)(C)?)|((MON|TUE|WED|THU|FRI|SAT|SUN)(-|/)(MON|TUE|WED|THU|FRI|SAT|SUN)(C)?)|(([1-7]|(MON|TUE|WED|THU|FRI|SAT|SUN))?(L|LW)?)|([1-7]#([1-7])?)|([\?])|([\*]))(([\s]19[7-9][0-9])|([\s]20[0-9]{2}))?
    @JsonProperty('Recurrence')
    String recurrence

    //TODO Change type to DateTime
    @JsonProperty('StartTime')
    String startTime

}
