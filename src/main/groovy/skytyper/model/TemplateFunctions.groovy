/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model


class TemplateFunctions {
    // Intrinsic Functions

    static def base64(String inputString) {
        [ 'Fn::Base64' : inputString ]
    }

    static def findInMap(Object... items) {
        [ 'Fn::FindInMap': items.collect { it } ]
    }

    static def getAtt(String resourceName, String attribute) {
        [ 'Fn::GetAtt': [ resourceName, attribute ] ]
    }

    static def getAZs() {
        [ 'Fn::GetAZs': "" ]
    }

    static def getAZs(String region) {
        [ 'Fn::GetAZs': region ]
    }

    static def join(String separator, Object... items) {
        [ 'Fn::Join': [ separator, items.collect { it } ] ]
    }

    static def select(String index, Object... items) {
        [ 'Fn::Select': [ index, items.collect { it } ] ]
    }

    static def ref(Resource resource) {
        [ 'Ref': resource.name ]
    }

    static def ref(Parameter parameter) {
        [ 'Ref': parameter.name ]
    }

    static def ref(String s) {
        [ 'Ref': s ]
    }

    // Pseudo parameters

    static def refAWSAccountId() {
        [ 'Ref': 'AWS::AccountId' ]
    }

    static def refAWSNotificationARNs() {
        [ 'Ref': 'AWS::NotificationARNs' ]
    }

    static def refAWSNoValue() {
        [ 'Ref': 'AWS::NoValue' ]
    }

    static def refAWSRegion() {
        [ 'Ref': 'AWS::Region' ]
    }

    static def refAWSStackId() {
        [ 'Ref': 'AWS::StackId' ]
    }

    static def refAWSStackName() {
        [ 'Ref': 'AWS::StackName' ]
    }
}
