/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.ec2

import skytyper.model.Template
import skytyper.model.TemplateFunctions

class EC2ResourceBuilder extends TemplateFunctions {

    private Template template

    EC2ResourceBuilder(Template template) {
        this.template = template
    }

    def securityGroup(String name, @DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = EC2SecurityGroup) Closure closure) {
        def newSG = new EC2SecurityGroup()
        newSG.name = name
        closure.delegate = newSG
        closure()
        template.add newSG
        newSG
    }

    def instance(String name, @DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = EC2Instance) Closure closure) {
        def newInstance = new EC2Instance()
        newInstance.name = name
        closure.delegate = newInstance
        closure()
        template.add newInstance
        newInstance
    }

    def elasticIP(String name, @DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = EIP) Closure closure) {
        def newResource = new EIP()
        newResource.name = name
        closure.delegate = newResource
        closure()
        template.add newResource
        newResource
    }

    def route(String name, @DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = Route) Closure closure) {
        def newResource = new Route(name)
        closure.delegate = newResource
        closure()
        template.add newResource
        newResource
    }

}
