/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.ec2

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue

enum InstanceType {
    // Current generation instance types
    T1_MICRO('t1.micro','Micro'),
    M1_SMALL('m1.small','M1 Small'),
    M3_MEDIUM('m3.medium','General Purpose Medium'),
    M3_LARGE('m3.large','General Purpose Large'),
    M3_XLARGE('m3.xlarge','General Purpose Extra Large'),
    M3_2XLARGE('m3.2xlarge','General Purpose Two Extra Large'),
    C3_LARGE('c3.large','Compute Optimized Large'),
    C3_XLARGE('c3.xlarge','Compute Optimized Extra Large'),
    C3_2XLARGE('c3.2xlarge','Compute Optimized Two Extra Large'),
    C3_4XLARGE('c3.4xlarge','Compute Optimized Four Extra Large'),
    C3_8XLARGE('c3.8xlarge','Compute Optimized Eight Extra Large'),
    R3_LARGE('r3.large','Memory Optimized Large'),
    R3_XLARGE('r3.xlarge','Memory Optimized Extra Large'),
    R3_2XLARGE('r3.2xlarge','Memory Optimized Two Extra Large'),
    R3_4XLARGE('r3.4xlarge','Memory Optimized Four Extra Large'),
    R3_8XLARGE('r3.8xlarge','Memory Optimized Eight Extra Large'),
    G2_2XLARGE('g2.2xlarge','GPU Optimized Two Extra Large'),
    I2_XLARGE('i2.xlarge','Storage Optimized Extra Large'),
    I2_2XLARGE('i2.2xlarge','Storage Optimized Two Extra Large'),
    I2_4XLARGE('i2.4xlarge','Storage Optimized Four Extra Large'),
    I2_8XLARGE('i2.8xlarge','Storage Optimized Eight Extra Large'),
    HS1_8XLARGE('hs1.8xlarge','Storage Optimized High Storage Eight Extra Large'),

    // Previous generation instance types
    M1_MEDIUM('m1.medium','Previous Generation - General Purpose Medium'),
    M1_LARGE('m1.large','Previous Generation - General Purpose Large'),
    M1_XLARGE('m1.xlarge','Previous Generation - General Purpose Extra Large'),
    C1_MEDIUM('c1.medium','Previous Generation - Compute Optimized Medium'),
    C1_XLARGE('c1.xlarge','Previous Generation - Compute Optimized Extra Large'),
    CC2_8XLARGE('cc2.8xlarge','Previous Generation - Compute Optimized Eight Extra Large'),
    CG1_4XLARGE('cg1.4xlarge','Previous Generation - GPU Four Extra Large'),
    HI1_4XLARGE('hi1.4xlarge','Previous Generation - Storage Optimized Four Extra Large'),
    M2_XLARGE('m2.xlarge','Previous Generation - Memory Optimized Extra Large'),
    M2_2XLARGE('m2.2xlarge','Previous Generation - Memory Optimized Two Extra Large'),
    M2_4XLARGE('m2.4xlarge','Previous Generation - Memory Optimized Four Extra Large'),
    CR1_8XLARGE('cr1.8xlarge','Previous Generation - Memory Optimized Eight Extra Large'),

    static final InstanceType DEFAULT = M1_SMALL
    final String name
    final String description

    InstanceType(String name, String description) {
        this.name = name
        this.description = description
    }

    @JsonValue
    String value() {
        name
    }

    @Override
    String toString() {
        value()
    }

    @JsonCreator
    static InstanceType fromValue(String typeString) {
        for (InstanceType t: values()) {
            if (t.value() == typeString.toLowerCase()) {
                return t;
            }
        }
        throw new IllegalArgumentException("Invalid InstanceType value: "+typeString)
    }
}
