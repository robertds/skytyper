/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.ec2

import skytyper.model.Resource
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import org.hibernate.validator.constraints.NotEmpty

import javax.validation.Valid
import javax.validation.constraints.NotNull


@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder([ "Type" ])
class EC2SecurityGroup extends Resource {
    static final String TYPE = "AWS::EC2::SecurityGroup"
    @JsonIgnore
    String getType() { TYPE }

    @NotEmpty
    @JsonIgnore
    String name = ''

    @NotNull
    @JsonProperty("Properties")
    private EC2SecurityGroupProperty properties

    //FIXME Refactor "createOutput" to a more appropriate place
    @JsonIgnore
    boolean createOutput = false

    EC2SecurityGroup() {
        properties = new EC2SecurityGroupProperty()
    }

    def properties(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = EC2SecurityGroupProperty) Closure closure) {
        def newSGP = new EC2SecurityGroupProperty()
        def clone = closure.rehydrate(newSGP, this, this)
        clone()
        this.properties = newSGP
        newSGP
    }

}

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class EC2SecurityGroupProperty {
    @JsonProperty("GroupDescription")
    String description = ''

    @Valid
    @JsonProperty("SecurityGroupEgress")
    List<EC2SecurityGroupRule> egressList = []

    @Valid
    @JsonProperty("SecurityGroupIngress")
    List<EC2SecurityGroupRule> ingressList = []

    @JsonProperty("VpcId")
    def vpcId = ''

    def ingress(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = EC2SecurityGroupRule) Closure closure) {
        def newSGR = new EC2SecurityGroupRule()
//        closure.delegate = newSGR
//        closure()
        def clone = closure.rehydrate(newSGR, this, this)
        clone()
        ingressList << newSGR
        newSGR
    }

    def egress(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = EC2SecurityGroupRule) Closure closure) {
        def newSGR = new EC2SecurityGroupRule()
//        closure.delegate = newSGR
//        closure()
        def clone = closure.rehydrate(newSGR, this, this)
        clone()
        egressList << newSGR
        newSGR
    }


}