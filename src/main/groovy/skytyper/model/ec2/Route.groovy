/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.ec2

import skytyper.model.Resource
import skytyper.validation.ValidCIDRBlock
import skytyper.validation.ValidRoute
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import org.hibernate.validator.constraints.NotEmpty

import javax.validation.Valid
import javax.validation.constraints.NotNull

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder([ "Type" ])
@ValidRoute
class Route extends Resource {
    static final String TYPE = "AWS::EC2::Route"
    @JsonIgnore
    String getType() { TYPE }

    @NotEmpty
    @JsonIgnore
    String name = ''

    @NotNull
    @JsonProperty("Properties")
    @Valid
    RouteProperties properties

    Route() {
        properties = new RouteProperties();
    }

    Route(String name) {
        this()
        this.name = name
    }

    def properties(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = RouteProperties) Closure closure) {
        def props = new RouteProperties()
        def clone = closure.rehydrate(props, this, this)
        clone()
        this.properties = props
        props
    }
}

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class RouteProperties {
    @NotNull
    @ValidCIDRBlock
    @JsonProperty('DestinationCidrBlock')
    String destinationCIDRBlock

    @JsonProperty('GatewayId')
    String gatewayID

    @JsonProperty('InstanceId')
    String instanceID

    @JsonProperty('NetworkInterfaceId')
    String networkInterfaceID

    @NotNull
    @JsonProperty('RouteTableId')
    String routeTableID
}