/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.ec2

import skytyper.model.Resource
import org.hibernate.validator.constraints.NotEmpty

import javax.validation.constraints.NotNull

class VPCGatewayAttachment extends Resource {
    //TODO Need class-level validation for skytyper.model.ec2.VPCGatewayAttachment
    static final String TYPE = "AWS::EC2::VPCGatewayAttachment"
    @NotEmpty
    String name = ''
    def internetGatewayID
    @NotNull
    def vpcID
    def vpnGatewayID

    def data() {
        def properties = [:]
        if (internetGatewayID) properties << ["InternetGatewayId": internetGatewayID ]
        if (vpcID) properties << ["VpcId": vpcID ]
        if (vpnGatewayID) properties << ["VpnGatewayId": vpnGatewayID ]
        [ "Type": TYPE, "Properties": properties ] }

    String getType() { TYPE }
}
