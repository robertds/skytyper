/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.ec2

import skytyper.model.Resource
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.validator.constraints.NotEmpty

import javax.validation.Valid
import javax.validation.constraints.NotNull

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class EIPAssociation extends Resource {
    //TODO Need class-level validation for skytyper.model.ec2.EIPAssociation
    static final String TYPE = "AWS::EC2::EIPAssociation"
    @JsonIgnore
    String getType() { TYPE }

    @JsonIgnore
    @NotEmpty
    String name = ''

    @NotNull
    @JsonProperty("Properties")
    @Valid
    EIPAssociationProperties properties

    EIPAssociation() {
        properties = new EIPAssociationProperties();
    }

    def properties(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = EIPAssociationProperties) Closure closure) {
        def props = new EIPAssociationProperties()
        def clone = closure.rehydrate(props, this, this)
        clone()
        this.properties = props
        props
    }

}

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class EIPAssociationProperties {
    @JsonProperty("AllocationId")
    def allocationID

    @JsonProperty("EIP")
    def eip

    @JsonProperty("InstanceId")
    def instanceID

    @JsonProperty("NetworkInterfaceId")
    def networkInterfaceID

}