/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.ec2

import skytyper.model.Resource
import skytyper.model.Tag
import skytyper.validation.ValidInstance
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.annotation.JsonValue
import org.hibernate.validator.constraints.NotEmpty

import javax.validation.Valid
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

@ValidInstance
@JsonInclude(Include.NON_EMPTY)
@JsonPropertyOrder([ "Type" ])
class EC2Instance extends Resource {
    static final String TYPE = "AWS::EC2::Instance"
    @JsonIgnore
    String getType() { TYPE }

    //TODO Implement stop/start capability
    @JsonIgnore
    boolean stoppable
    @JsonIgnore
    boolean startable

    @NotEmpty
    @JsonIgnore
    String name

    @NotNull
    @JsonProperty("Properties")
    @Valid
    EC2InstanceProperties properties

    EC2Instance() {
        properties = new EC2InstanceProperties();
    }

    def properties(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = EC2InstanceProperties) Closure closure) {
        def props = new EC2InstanceProperties()
        def clone = closure.rehydrate(props, this, this)
        clone()
        this.properties = props
        props
    }

}

@JsonInclude(Include.NON_EMPTY)
class EC2InstanceProperties {
    @JsonProperty("AvailabilityZone")
    def availabilityZone

    @JsonProperty("BlockDeviceMappings")
    BlockDeviceMappingProperty[] blockDeviceMappings = []

    @JsonProperty("DisableApiTermination")
    def disableAPITermination = false

    @JsonProperty("EbsOptimized")
    def ebsOptimized = false

    @JsonProperty("IamInstanceProfile")
    def iamInstanceProfile

    @NotNull
    @JsonProperty("ImageId")
    def imageID

    @NotNull
    @JsonProperty("InstanceType")
    def instanceType = InstanceType.DEFAULT

    @JsonProperty("KernelId")
    def kernelID

    @JsonProperty("KeyName")
    def keyName

    @JsonProperty("Monitoring")
    def monitoring = false

    @JsonProperty("NetworkInterfaces")
    NetworkInterfaceProperty[] networkInterfaces = []

    @JsonProperty("PlacementGroupName")
    def placementGroupName

    @JsonProperty("PrivateIpAddress")
    def privateIPAddress

    @JsonProperty("RamdiskId")
    def ramDiskID

    @JsonProperty("SecurityGroupIds")
    def securityGroupIDs = []

    @JsonProperty("SecurityGroups")
    def securityGroups = []

    @JsonProperty("SourceDestCheck")
    def sourceDestCheck = false

    @JsonProperty("SubnetId")
    def subnetID

    @Valid
    @JsonProperty("Tags")
    Tag[] tags

    @JsonProperty("Tenancy")
    def tenancy

    @JsonProperty("UserData")
    def userData

    @Valid
    @JsonProperty("Volumes")
    MountPointProperty[] volumes = []
}

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class BlockDeviceMappingProperty {
    //TODO Add class validator for BlockDeviceMappingProperty
    @NotEmpty
    @JsonProperty("DeviceName")
    String deviceName

    @JsonProperty("Ebs")
    EBSBlockDeviceProperty ebsBlockDevice

    @JsonProperty("NoDevice")
    boolean noDevice = false
    boolean isNoDevice() { this.noDevice }

    @Pattern(regexp = /^ephemeral\d+$/)
    @JsonProperty("VirtualName")
    String virtualName
}

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class EBSBlockDeviceProperty {
    @JsonProperty("DeleteOnTermination")
    boolean deleteOnTermination = true
    boolean isDeleteOnTermination() { this.deleteOnTermination }

    //FIXME This validation is tricky, need class level validation
    @Min(value = 100L)
    @Max(value = 2000L)
    @JsonProperty("Iops")
    Integer iops

    @JsonProperty("SnapshotId")
    String snapshotID

    @Min(value = 1L)
    @Max(value = 1024L)
    @JsonProperty("VolumeSize")
    Integer volumeSize

    @JsonProperty("VolumeType")
    VolumeType volumeType

    enum VolumeType {
        STANDARD('standard'),
        IO1('io1')
        String value

        VolumeType(String value) {
            this.value = value
        }
        @JsonValue
        String value() {
            value
        }

        @JsonCreator
        static VolumeType fromValue(String typeString) {
            for (VolumeType t: values()) {
                if (t.value() == typeString.toLowerCase()) {
                    return t;
                }
            }
            throw new IllegalArgumentException("Invalid VolumeType value: "+typeString)
        }
    }

}
