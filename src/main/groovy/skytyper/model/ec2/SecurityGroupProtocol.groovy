/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.ec2

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue

enum SecurityGroupProtocol {
    TCP('tcp',6), UDP('udp',17), ICMP('icmp',1), ALL('-1',-1)

    final Integer value
    final String name

    SecurityGroupProtocol(String name, Integer value) {
        this.name = name
        this.value = value
    }

    @JsonValue
    String value() {
        name
    }

    @Override
    String toString() {
        name
    }

    @JsonCreator
    static SecurityGroupProtocol fromValue(String typeString) {
        for (SecurityGroupProtocol t: values()) {
            if (t.value() == typeString.toLowerCase()) {
                return t;
            }
        }
        throw new IllegalArgumentException("Invalid SecurityGroupProtocol value: "+typeString)
    }

    SecurityGroupProtocol(int protocolValue) {
        this.value = protocolValue
    }
}