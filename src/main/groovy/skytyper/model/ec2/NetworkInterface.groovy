/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.ec2

import skytyper.model.Resource
import skytyper.model.Tag
import org.hibernate.validator.constraints.NotEmpty

import javax.validation.constraints.NotNull

class NetworkInterface extends Resource {
    static final String TYPE = "AWS::EC2::NetworkInterface"
    @NotEmpty
    String name = ''
    String description
    def groupSet = []
    def privateIPAddress
    boolean sourceDestCheck = false
    @NotNull
    def subnetID
    Tag[] tags

    def data() {
        def properties = [:]
        if (description) properties << ["Description": description ]
        if (groupSet) properties << ["GroupSet": groupSet.collect { it } ]
        if (privateIPAddress) properties << ["PrivateIpAddress": privateIPAddress ]
        properties << ["SourceDestCheck": sourceDestCheck ]
        if (subnetID) properties << ["SubnetId": subnetID ]
        if (tags) properties << ["Tags": tags.collect{ it.data() } ]
        [ "Type": TYPE, "Properties": properties ] }

    String getType() { TYPE }
}
