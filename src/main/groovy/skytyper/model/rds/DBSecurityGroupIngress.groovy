/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.rds

import skytyper.validation.ValidCIDRBlock

class DBSecurityGroupIngress {
    @ValidCIDRBlock
    def cidrIP = ''
    String ec2SecurityGroupId = ''
    String ec2SecurityGroupName = ''
    String ec2SecurityGroupOwnerId = ''

    def data() {
        def dataMap = [:]
        if (cidrIP) dataMap << [ 'CIDRIP': cidrIP ]
        if (ec2SecurityGroupId) dataMap << [ 'EC2SecurityGroupId': ec2SecurityGroupId ]
        if (ec2SecurityGroupName) dataMap << [ 'EC2SecurityGroupName': ec2SecurityGroupName ]
        if (ec2SecurityGroupOwnerId) dataMap << [ 'EC2SecurityGroupOwnerId': ec2SecurityGroupOwnerId ]
        dataMap
    }

}
