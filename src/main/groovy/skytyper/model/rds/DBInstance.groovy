/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.rds

import skytyper.model.Resource
import skytyper.model.Tag
import skytyper.validation.ValidDBInstance
import org.hibernate.validator.constraints.NotEmpty

import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

@ValidDBInstance
class DBInstance extends Resource {
    static final String TYPE = "AWS::RDS::DBInstance"
    @NotEmpty
    String name
    @Min(value = 1L)
    int allocatedStorage
    boolean autoMinorVersionUpgrade = true
    String availabilityZone
    int backupRetentionPeriod
    @NotNull
    DBInstanceClass dbInstanceClass
    String dbName
    String dbParameterGroupName
    def dbSecurityGroups = []
    String dbSnapshotIdentifier
    def dbSubnetGroupName
    @NotNull
    DBEngine engine
    String engineVersion
    int iops
    String licenseModel
    @NotEmpty
    String masterUsername
    @NotEmpty
    String masterUserPassword
    boolean multiAZ = false
    Integer port = null
    String preferredBackupWindow
    String preferredMaintenanceWindow
    Tag[] tags = []
    def vpcSecurityGroups = []

    def data() {
        def properties = [:]
        if (allocatedStorage) properties << [ "AllocatedStorage": allocatedStorage ]
        properties << [ "AutoMinorVersionUpgrade": autoMinorVersionUpgrade ]
        if (availabilityZone) properties << [ "AvailabilityZone": availabilityZone ]
        if (backupRetentionPeriod) properties << [ "BackupRetentionPeriod": backupRetentionPeriod ]
        if (dbInstanceClass) properties << [ "DBInstanceClass": dbInstanceClass.name ]
        if (dbName) properties << [ "DBName": dbName ]
        if (dbParameterGroupName) properties << [ "DBParameterGroupName": dbParameterGroupName ]
        if (dbSecurityGroups) properties << [ "DBSecurityGroups": dbSecurityGroups.collect { it } ]
        if (dbSnapshotIdentifier) properties << [ "DBSnapshotIdentifier": dbSnapshotIdentifier ]
        if (dbSubnetGroupName) properties << [ "DBSubnetGroupName": dbSubnetGroupName ]
        if (engine) properties << [ "Engine": engine.value ]
        if (engineVersion) properties << [ "EngineVersion": engineVersion ]
        if (iops) properties << [ "Iops": iops ]
        if (licenseModel) properties << [ "LicenseModel": licenseModel ]
        if (masterUsername) properties << [ "MasterUsername": masterUsername ]
        if (masterUserPassword) properties << [ "MasterUserPassword": masterUserPassword ]
        if (multiAZ) properties << [ "MultiAZ": multiAZ ]
        if (port) properties << [ "Port": port ]
        if (preferredBackupWindow) properties << [ "PreferredBackupWindow": preferredBackupWindow ]
        if (preferredMaintenanceWindow) properties << [ "PreferredMaintenanceWindow": preferredMaintenanceWindow ]
        if (tags) properties << [ "tags": tags.collect { it.data() } ]
        if (vpcSecurityGroups) properties << [ "VPCSecurityGroups": vpcSecurityGroups.collect { it } ]
        [ "Type": TYPE, "Properties": properties ]
    }

    String getType() { TYPE }
}

