/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.rds

enum DBInstanceClass {
    DB_T1_MICRO('db.t1.micro','Micro DB instance'),
    DB_M1_SMALL('db.m1.small','Small DB instance'),
    DB_M1_MEDIUM('db.m1.medium','Medium DB instance'),
    DB_M1_LARGE('db.m1.large','Large DB instance'),
    DB_M1_XLARGE('db.m1.xlarge','Extra Large DB instance'),
    DB_M2_XLARGE('db.m2.xlarge','High-Memory Extra Large DB instance'),
    DB_M2_2XLARGE('db.m2.2xlarge','High-Memory Double Extra Large DB instance'),
    DB_M2_4XLARGE('db.m2.4xlarge','High-Memory Quadruple Extra Large DB instance')

    final String name
    final String description

    DBInstanceClass(String name, String description) {
        this.name = name
        this.description = description
    }
}