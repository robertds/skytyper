/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model

class ParameterBuilder {

    private Template template

    ParameterBuilder(Template template) {
        this.template = template
    }

    def StringParameter string(String name, @DelegatesTo(strategy = Closure.DELEGATE_ONLY, value = StringParameter) Closure closure) {
        def stringParameter = new StringParameter()
        stringParameter.name = name
        closure.delegate = stringParameter
        closure()
        template.add stringParameter
        stringParameter
    }

    def NumberParameter number(String name, @DelegatesTo(strategy = Closure.DELEGATE_ONLY, value = NumberParameter) Closure closure) {
        def numberParameter = new NumberParameter()
        numberParameter.name = name
        closure.delegate = numberParameter
        closure()
        template.add numberParameter
        numberParameter
    }


}
