/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder([ "Type" ])
class NumberParameter extends Parameter {
    static final String TYPE = 'Number'
    @JsonIgnore
    String getType() { TYPE }
    private setType() {}

    @JsonProperty("MaxValue")
    int maxValue
    @JsonProperty("MinValue")
    int minValue

    def data() {
        def dataMap = [:]
        dataMap << [ 'Type': TYPE ]
        if (defaultValue) dataMap << [ 'Default': defaultValue ]
        if (noEcho) dataMap << [ 'NoEcho': noEcho ]
        if (allowedValues) dataMap << [ 'AllowedValues': allowedValues ]
        if (maxValue) dataMap << [ 'MaxValue': maxValue ]
        if (minValue) dataMap << [ 'MinValue': minValue ]
        if (description) dataMap << [ 'Description': description ]
        if (constraintDescription) dataMap << [ 'ConstraintDescription': constraintDescription ]
        dataMap
    }

}
