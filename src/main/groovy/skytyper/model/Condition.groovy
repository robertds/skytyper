/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonValue
import org.hibernate.validator.constraints.NotEmpty

import javax.validation.constraints.NotNull

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class Condition {
    //FIXME Not sure if this will work as written, need unit tests
    @NotEmpty
    @JsonIgnore
    String name = ''

    @NotNull
    @JsonIgnore
    def value

    @JsonValue
    Object value() {
        value
    }

    @JsonCreator
    static Condition fromObject(Object o) {
        new Condition(value: o)
    }

    static HashMap fnAnd(Object... conditions) {
        [ 'Fn::And' : conditions.collect { it } ]
    }

    static HashMap fnEquals(Object o1, Object o2) {
        [ 'Fn::Equals' : [ o1, o2 ] ]
    }

    static HashMap fnIf(Object conditionName, Object valueIfTrue, Object valueIfFalse) {
        [ 'Fn::If' : [ conditionName, valueIfTrue, valueIfFalse ] ]
    }

    static HashMap fnNot(Object condition) {
        [ 'Fn::Not' :  condition ]
    }

    static HashMap fnOr(Object... conditions) {
        [ 'Fn::Or' : conditions.collect { it } ]
    }


}
