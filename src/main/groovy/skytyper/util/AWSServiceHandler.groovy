/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.util

import com.amazonaws.AmazonClientException
import com.amazonaws.AmazonServiceException
import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.AWSCredentialsProviderChain
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.auth.SystemPropertiesCredentialsProvider
import com.amazonaws.services.cloudformation.AmazonCloudFormationClient
import com.amazonaws.services.cloudformation.model.*
import com.amazonaws.services.sns.AmazonSNSClient
import com.amazonaws.services.sns.model.CreateTopicRequest
import com.amazonaws.services.sns.model.DeleteTopicRequest
import com.amazonaws.services.sns.util.Topics
import com.amazonaws.services.sqs.AmazonSQSClient
import com.amazonaws.services.sqs.model.DeleteMessageBatchRequest
import com.amazonaws.services.sqs.model.DeleteMessageBatchRequestEntry
import com.amazonaws.services.sqs.model.DeleteQueueRequest
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest
import com.amazonaws.services.sqs.model.ReceiveMessageRequest
import skytyper.model.Template
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j

import static org.fusesource.jansi.Ansi.*

@Slf4j
class AWSServiceHandler {
    static final String SKYTYPER_NOTIFICATION_NAME = 'SkyTyperNotifications'
    static final int REQUEST_WAIT_TIME_SECONDS = 20
    static final int REQUEST_MAX_NUM_MESSAGES = 10
    static final int REQUEST_VISIBILITY_TIMEOUT = 5
    private AWSCredentials awsCredentials
    private AmazonCloudFormationClient cloudFormationClient
    private AmazonSNSClient snsClient
    private AmazonSQSClient sqsClient
    private String queueURL
    private String topicARN

    AWSServiceHandler() throws Exception {
        //TODO Add BasicAWSCredentials support for using the config file for credentials
        try {
            this.awsCredentials = new AWSCredentialsProviderChain(
                    new ClasspathPropertiesFileCredentialsProvider(),
                    new EnvironmentVariableCredentialsProvider(),
                    new SystemPropertiesCredentialsProvider()).getCredentials()

        } catch (AmazonClientException ignored) {
            throw new Exception("Unable to load AWS credentials from file on classpath, environment variables, or system properties.")
        }
    }

    Map<String, String> createStack(String fullStackName, Template template, Map<String, Object> runtimeParameters,
                                    Collection<String> capabilities) {

        Stack cfStack = null
        List<Parameter> parameters = (runtimeParameters) ? getMapAsParameters(runtimeParameters) : new ArrayList<Parameter>()

        try {
            log.debug "Checking for stack [$fullStackName]"
            List<Stack> cfStacks = getCloudFormationClient().describeStacks(
                    new DescribeStacksRequest().withStackName(fullStackName)).getStacks()
            if (cfStacks) cfStack = cfStacks.find { it.stackName == fullStackName }
            if (cfStack?.stackStatus == StackStatus.CREATE_FAILED.toString()) {
                log.warn "   Found previously failed stack, deleting..."
                deleteStack(fullStackName)
                cfStack = null
            }
        } catch (AmazonServiceException e) {
            if (e.errorCode == 'ValidationError') {
                log.debug "Stack $fullStackName not found"
            } else {
                log.error "${e.serviceName} ${e.errorCode}: ${e.message}"
            }
        }


        if (cfStack) {
            log.info(ansi().render(" @|bold,yellow -|@ Found stack @|bold $fullStackName|@, skipping create").toString())
            parameters += cfStack.outputs.collect {
                new Parameter()
                        .withParameterKey(it.outputKey)
                        .withParameterValue(it.outputValue)
            }
        } else {
            log.info(ansi().render(" @|bold \u25CF|@ Creating stack @|bold $fullStackName|@...").toString())
            try {
                CreateStackRequest request = new CreateStackRequest()
                        .withTemplateBody(new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(template))
                        .withDisableRollback(true)
                        .withStackName(fullStackName)
                        .withNotificationARNs(getNotificationTopic())
                        .withCapabilities(capabilities)

                // Workaround for CloudFormation feature that rejects non-defined parameters passed into stacks
                if (parameters) {
                   template.parameters.each { p ->
                       request.parameters += parameters.find { p.key == it.parameterKey }
                    }
                }

                getCloudFormationClient().createStack(request)

                List<Stack> cfStacks = getCloudFormationClient().describeStacks(
                        new DescribeStacksRequest().withStackName(fullStackName)).getStacks()
                if (cfStacks) cfStack = cfStacks.find { it.stackName == fullStackName }
                if (cfStack) {
                    //TODO Add comma-delimited parser feature
                    log.debug "Loading outputs into runtime parameters for stack [$fullStackName]"
                    parameters += cfStack.outputs.collect {
                        new Parameter()
                                .withParameterKey(it.outputKey)
                                .withParameterValue(it.outputValue)
                    }
                }

                // Monitor queue and log output
                handleEventLogging("CREATE", fullStackName)

                // Query outputs
                //TODO Refactor this snippet, repeated code
                cfStacks = getCloudFormationClient().describeStacks(
                        new DescribeStacksRequest().withStackName(fullStackName)).getStacks()
                if (cfStacks) cfStack = cfStacks.find { it.stackName == fullStackName }
                if (cfStack) {
                    //TODO Add comma-delimited parser feature
                    log.debug "Loading outputs into runtime parameters for stack [$fullStackName]"
                    parameters += cfStack.outputs.collect {
                        new Parameter()
                                .withParameterKey(it.outputKey)
                                .withParameterValue(it.outputValue)
                    }
                }

                log.info(ansi().render(" @|bold,green \u2714|@ Request to create @|bold $fullStackName|@ completed").toString())
            } catch (AmazonServiceException e) {
                if (e.errorCode == 'ValidationError') {
                    log.error(ansi().render(" @|bold,red \u2717|@ Stack @|bold $fullStackName|@ is @|red,bold not valid|@: ${e.message}").toString())
                } else {
                    log.error "${e.serviceName} ${e.errorCode}: ${e.message}"
                }
            }
        }
        getParametersAsMap(parameters)
    }

    Map<String, String> updateStack(String fullStackName, Template template, Map<String, Object> runtimeParameters, Collection<String> capabilities) {
        def cfStack = null
        List<Parameter> parameters = getMapAsParameters(runtimeParameters)

        log.info(ansi().render(" @|bold \u25CF|@ Updating stack @|bold ${fullStackName}|@...").toString())
        try {
            UpdateStackRequest request = new UpdateStackRequest()
                    .withCapabilities(capabilities)
                    .withTemplateBody(new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(template))
                    .withStackName(fullStackName)


            // Workaround for CloudFormation feature that rejects non-defined parameters passed into stacks
            if (parameters) {
                template.parameters.each { p ->
                    request.parameters += parameters.find { p.key == it.parameterKey }
                }
            }

            getCloudFormationClient().updateStack(request)

            handleEventLogging("UPDATE", fullStackName)

            List<Stack> cfStacks = getCloudFormationClient().describeStacks(
                    new DescribeStacksRequest().withStackName(fullStackName)).getStacks()
            if (cfStacks) cfStack = cfStacks.find { it.stackName == fullStackName }
            if (cfStack) {
                parameters += loadParametersFromStack(fullStackName)
            }
        } catch (AmazonServiceException e) {
            if (e.errorCode == 'ValidationError') {
                log.info(ansi().render(" @|bold,yellow -|@ Skipping update for stack @|bold ${fullStackName}|@: ${e.errorMessage}").toString())
            } else {
                log.error "${e.serviceName} ${e.errorCode}: ${e.message}"
            }
        }
        log.info(ansi().render(" @|bold,green \u2714|@ Request to update @|bold ${fullStackName}|@ completed").toString())
        getParametersAsMap(parameters)
    }

    def deleteStack(String fullStackName) {
        Stack cfStack
        try {
            log.debug "Checking for stack $fullStackName"
            List<Stack> cfStacks = getCloudFormationClient().describeStacks(
                    new DescribeStacksRequest().withStackName("$fullStackName")).getStacks()
            if (cfStacks) cfStack = cfStacks.find { it.stackName == fullStackName }
        } catch (AmazonServiceException e) {
            if (e.errorCode == 'ValidationError') {
                log.info(ansi().render(" @|bold,yellow -|@ Stack $fullStackName not found, skipping delete").toString())
            } else {
                log.error "${e.serviceName} ${e.errorCode}: ${e.message}"
            }
        }

        if (cfStack) {
            log.info(ansi().render(" @|bold \u25CF|@ Deleting stack @|bold $fullStackName|@").toString())
            try {
                getCloudFormationClient().deleteStack(new DeleteStackRequest()
                        .withStackName(fullStackName))

                // Monitor queue here and log output
                handleEventLogging("DELETE", fullStackName)

                log.info(ansi().render(" @|bold,green \u2714|@ Request to delete @|bold $fullStackName|@ completed").toString())
            } catch (AmazonServiceException e) {
                log.error "${e.serviceName} ${e.errorCode}: ${e.message}"
            }
        }
    }

    def validateStack(String fullStackName, Template template) {
        try {
            getCloudFormationClient().validateTemplate(new ValidateTemplateRequest().withTemplateBody(template.toString()))
            log.info (ansi().render(" @|bold,green \u2714|@ CloudFormation indicates stack @|bold ${fullStackName}|@ is valid").toString())
        } catch (AmazonServiceException e) {
            if (e.errorCode == 'ValidationError') {
                log.warn (ansi().render(" @|bold,red \u2717|@ CloudFormation indicates stack @|bold ${fullStackName}|@ is @|bold,red not valid|@: ${e.message}").toString())
            } else {
                log.error "${e.serviceName} ${e.errorCode}: ${e.message}"
            }
        }

    }

    private def handleEventLogging(String action, String fullStackName) {
        boolean stackComplete = false
        def jsonSlurper = new JsonSlurper()
        while (!stackComplete) {
            def messages = getSqsClient().receiveMessage(new ReceiveMessageRequest()
                    .withMaxNumberOfMessages(REQUEST_MAX_NUM_MESSAGES)
                    .withWaitTimeSeconds(REQUEST_WAIT_TIME_SECONDS)
                    .withVisibilityTimeout(REQUEST_VISIBILITY_TIMEOUT)
                    .withQueueUrl(getQueueURL())).messages

            def filteredMessages = messages.findAll {
                def jsonMessage = jsonSlurper.parseText(it.body)
                String messageString = jsonMessage['Message']
                def attributeMap = messageString.split('\n').inject([:]) { map, token ->
                    token.split('=').with { map[ it[0] ] = it[1].trim().replaceAll('\'','') }
                    map
                }
                attributeMap['StackName'] == fullStackName
            }

            filteredMessages.each {
                def jsonMessage = jsonSlurper.parseText(it.body)
                String messageString = jsonMessage['Message']
                def attributeMap = messageString.split('\n').inject([:]) { map, token ->
                    token.split('=').with { map[ it[0] ] = it[1].trim().replaceAll('\'','') }
                    map
                }
                logMessageResults(attributeMap)
                if (attributeMap['ResourceType'] == 'AWS::CloudFormation::Stack') {
                    if ((attributeMap['ResourceStatus'] == "${action}_COMPLETE") ||
                            (attributeMap['ResourceStatus'] == "${action}_FAILED")) stackComplete = true
                }
            }
            if (filteredMessages) {
                getSqsClient().deleteMessageBatch(new DeleteMessageBatchRequest(getQueueURL()).withEntries(
                        filteredMessages.collect {
                            new DeleteMessageBatchRequestEntry()
                                    .withId(it.messageId)
                                    .withReceiptHandle(it.receiptHandle)
                        }))
            }
        }

    }



    String getNotificationTopic() {
        // Lookup existing topic and queue

        if (!topicARN) {
            def topics = getSnsClient().listTopics().topics
            def foundTopicArn = topics.find { it.topicArn.split(':')[5] == SKYTYPER_NOTIFICATION_NAME }?.topicArn
            if (!foundTopicArn) {
                topicARN = getSnsClient().createTopic(new CreateTopicRequest().withName(SKYTYPER_NOTIFICATION_NAME)).topicArn
            } else {
                topicARN = foundTopicArn
            }
        }


        // Check to ensure Queue is subscribed to Topic

        def subscriptions = getSnsClient().listSubscriptions().subscriptions
        def subscriptionFound = subscriptions.find { (it.topicArn == topicARN) && (it.endpoint == getQueueARN()) }
        if (!subscriptionFound) {
            //Note this includes a workaround for a bug (System.out.println) in the AWS Java SDK Topic class
            PrintStream originalStream = System.out
            PrintStream noopStream = new PrintStream(new OutputStream() {
                @Override
                void write(int b) throws IOException {
                    // Ignore write
                }
            })
            System.setOut(noopStream)
            Topics.subscribeQueue(getSnsClient(), getSqsClient(), topicARN, getQueueURL())
            System.setOut(originalStream)
        }

        topicARN
    }

    List<Parameter> loadParametersFromStack(String fullStackName) {
        List<Parameter> parameters = new ArrayList<Parameter>()
        Stack cfStack
        try {
            //TODO Refactor this snippet, repeated code
            def cfStacks = getCloudFormationClient().describeStacks(
                    new DescribeStacksRequest().withStackName(fullStackName)).getStacks()
            if (cfStacks) cfStack = cfStacks.find { it.stackName == fullStackName }
            if (cfStack) {
                //TODO Add comma-delimited parser feature
                log.debug "Loading outputs into runtime parameters for stack [${fullStackName}]"
                parameters += cfStack.outputs.collect {
                    new Parameter()
                            .withParameterKey(it.outputKey)
                            .withParameterValue(it.outputValue)
                }
            }
        } catch (AmazonServiceException e) {
            if (e.errorCode == 'ValidationError') {
                log.error "Stack [${fullStackName}] is not found: ${e.message}"
            } else {
                log.error "${e.serviceName} ${e.errorCode}: ${e.message}"
            }
        }
        parameters
    }


    def tearDownNotificationQueue() {
        getNotificationTopic()
        getSnsClient().deleteTopic(new DeleteTopicRequest(topicARN))
        getSqsClient().deleteQueue(new DeleteQueueRequest(queueURL))
        topicARN = null
        queueURL = null
    }

    String getQueueARN() {
        getSqsClient().getQueueAttributes(new GetQueueAttributesRequest().withAttributeNames("QueueArn")
                .withQueueUrl(getQueueURL())).getAttributes()['QueueArn']
    }

    String getQueueURL() {
        if (!queueURL) {
            def queueURLs = getSqsClient().listQueues().queueUrls
            def foundQueueURL = queueURLs.find { it.split('/')[4] == SKYTYPER_NOTIFICATION_NAME }
            if (!foundQueueURL) {
                queueURL = getSqsClient().createQueue(SKYTYPER_NOTIFICATION_NAME).queueUrl
            } else {
                queueURL = foundQueueURL
            }
        } else {
            queueURL
        }
    }

    /*
     * Lazy load the SNS Client
     */
    AmazonSNSClient getSnsClient() {
        if (!snsClient) {
            snsClient = new AmazonSNSClient(awsCredentials)
        } else {
            snsClient
        }
    }

    /*
     * Lazy load the SQS Client
     */
    AmazonSQSClient getSqsClient() {
        if (!sqsClient) {
            sqsClient = new AmazonSQSClient(awsCredentials)
        } else {
            sqsClient
        }
    }

    /*
     * Lazy load the CloudFormation client
     */
    AmazonCloudFormationClient getCloudFormationClient() {
        if (!cloudFormationClient) {
            cloudFormationClient = new AmazonCloudFormationClient(awsCredentials)
        } else {
            cloudFormationClient
        }
    }

    static Map<String, Object> getParametersAsMap(List<Parameter> parameters) {
        def parameterMap = new HashMap<String, String>()
        parameters.collectEntries { [ it.parameterKey, it.parameterValue ] }
        parameterMap
    }

    static List<Parameter> getMapAsParameters(Map<String, Object> parameters) {
        parameters.collect { new Parameter().withParameterKey(it.key).withParameterValue(it.value.toString())  }
    }

    static def logMessageResults(attributeMap) {
        if (attributeMap['ResourceStatus'].toString().contains("FAILED")) {
            log.error(ansi().render("   @|bold,red ${humanizeResult(attributeMap['ResourceStatus'])}|@  for " +
                    "${attributeMap['ResourceType']} named @|bold ${attributeMap['LogicalResourceId']}|@: " +
                    "${attributeMap['ResourceStatusReason']}").toString())
        } else if (attributeMap['ResourceStatus'].toString().contains("COMPLETE")) {
            log.info(ansi().render("   @|bold,green ${humanizeResult(attributeMap['ResourceStatus'])}|@ for " +
                    "${attributeMap['ResourceType']} named @|bold ${attributeMap['LogicalResourceId']}|@ " +
                    "(${attributeMap['PhysicalResourceId']})").toString())
        } else {
            log.info(ansi().render("   @|bold ${humanizeResult(attributeMap['ResourceStatus'])}|@ for " +
                    "${attributeMap['ResourceType']} named @|bold ${attributeMap['LogicalResourceId']}|@ " +
                    "${attributeMap['ResourceStatusReason'] ? attributeMap['ResourceStatusReason'] : ''}").toString())
        }
    }

    static String humanizeResult(Object result) {
        String humanized = result.toString().replaceAll('_',' ')
        humanized.toLowerCase().capitalize()
    }
}
