/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.util

import skytyper.script.ConfigParser
import skytyper.script.ScriptAction
import skytyper.script.SkyTyperScript
import com.typesafe.config.Config
import groovy.util.logging.Slf4j
import org.joda.time.DateTime
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.PeriodFormatter
import org.joda.time.format.PeriodFormatterBuilder

@Slf4j
//@groovy.transform.TypeChecked
class Runner {
    static final String VERSION = '0.9'
    static final String DEFAULT_CONFIG_FILENAME = 'skytyper.conf'
    static final String CLI_HEADER = "\nSkyTyper: CloudFormation productivity tool. A default config file named $DEFAULT_CONFIG_FILENAME will be used if located in the current directory. " +
                "Available commands are: init, checkmodel, validate, create, update, delete, deleteCreate, start, stop, export, print, summary, clean, printconfig, help\nOptions:"
    static final String CLI_FOOTER = " \n \n"
    static final def DATE_FORMATTER = DateTimeFormat.forPattern('yyyy-MM-dd HH:mm:ss z')
    static final PeriodFormatter PERIOD_FORMATTER = new PeriodFormatterBuilder()
        .appendDays()
        .appendSuffix(" day", " days")
        .appendSeparatorIfFieldsBefore(", ")
        .appendHours()
        .appendSuffix(" hour", " hours")
        .appendSeparatorIfFieldsBefore(", ")
        .appendMinutes()
        .appendSuffix(" minute", " minutes")
        .appendSeparatorIfFieldsBefore(" and ")
        .appendSecondsWithOptionalMillis()
        .appendSuffix(" seconds").toFormatter()

    private static CliBuilder cli
    private static OptionAccessor options
    private static String commandArg
    private static ScriptAction action
    private static String helpTopic
    private static String configFilename
    private static String label
    private static Config config
    private static Boolean disableNotify = false
    private SkyTyperScript script

    Runner(args) {
        cli = new CliBuilder(usage: 'skytyper [options] <command>', header: CLI_HEADER, footer: CLI_FOOTER)
        cli.with {
            l(longOpt: 'label', 'Specify which label to use as defined in the config file', args: 1, type: String, required: false)
            c(longOpt: 'config', "Specify an optional config filename. Default config filename is 'skytyper.conf'", args: 1, type: String, required: false)
            n(longOpt: 'nonotify', "Run asynchronously and disable notifications", args: 1, type: Boolean, required: false)
//            d(longOpt: 'debug', 'Show extra verbose output for debugging', args: 1, type: String, required: false)
            //TODO Document command line parameter support
        }
        options = cli.parse(args)
        if (!options) {
            System.exit(0)
        }
        if ((!options.arguments()) || options.arguments().size() > 2) {
            cli.usage()
            System.exit(0)
        }
        commandArg = options.arguments()[0]

        if (options.config) {
            configFilename = options.config
        } else {
            configFilename = DEFAULT_CONFIG_FILENAME
        }

        if (options.label) {
            label = options.label
        }

        //TODO Implement nonotify with operations
        if (options.nonotify) {
            disableNotify = true
        }

//        if (options.debug) {
//            //TODO Add super-verbose mode that includes stack export, parameter inputs/outputs, (i.e. to help debug)
//            log.warn("Debug option not implemented.")
//        }


    }

    void run() {
        printWelcome()
        DateTime startTime = DateTime.now()

        switch(commandArg) {
            case 'help':
                if (options.arguments()[1] != null) helpTopic = options.arguments()[1]
                helpSwitch()
                break
            case 'printconfig':
                config = ConfigParser.getConfig(configFilename)
                printConfig()
                break
            case 'init':
                initializeProject()
                break
            default:
                try {
                    config = ConfigParser.getConfig(configFilename)
                    action = ScriptAction.fromValue(commandArg)
                    script = ConfigParser.generate(config, label)
                    println "${DATE_FORMATTER.print(new DateTime())} - $commandArg\n"
                    script.run(action)
                } catch (IllegalArgumentException ignore) {
                    log.error("Invalid action argument passed: "+commandArg)
                    cli.usage()
                     System.exit(1)
                }
        }

        DateTime endTime = DateTime.now()
        Period period = new Period(startTime,endTime)
        println "\n${DATE_FORMATTER.print(new DateTime())} - Finished $commandArg in ${PERIOD_FORMATTER.print(period)}"
    }

    static main(args) {
        def runner = new Runner(args)
        runner.run()
    }

    static void initializeProject() {
        //TODO Create default directories and config file
        println "TODO: implement init command"
    }

    static void printWelcome() {
        println "\nSkyTyper - CloudFormation Productivity Tool v$VERSION"
        println '------------------------------------------------------------'
    }

    static void printConfig() {
        println config.getObject('skytyper').render()
    }

    static void helpSwitch() {
        switch (helpTopic) {
            case 'help':
                printFullHelp()
                break
            case 'init':
                printInitHelp()
                break
            case 'create':
                printCreateHelp()
                break
            case 'update':
                printUpdateHelp()
                break
            case 'delete':
                printDeleteHelp()
                break
            case 'createDelete':
                printCreateDeleteHelp()
                break
            case 'start':
                printStartHelp()
                break
            case 'stop':
                printStopHelp()
                break
            case 'print':
                printPrintHelp()
                break
            case 'export':
                printExportHelp()
                break
            case 'summary':
                printSummaryHelp()
                break
            case 'checkmodels':
                printCheckModelsHelp()
                break
            case 'validate':
                printValidateHelp()
                break
            case 'clean':
                printCleanHelp()
                break
            case 'showconfig':
                printShowConfigHelp()
                break
            default:
                printFullHelp()
        }
        System.exit(0)
    }

    static void printFullHelp() {
        println '''The following commands are available:

 help           - This help text
 help <command> - Detailed help for a given <command>
 init           - Create a standard project layout and default config file in
                  the current directory if nonexistent
 create         - Run 'create' with the SkyTyper script
 update         - Run 'update' with the SkyTyper script
 delete         - Run 'delete' with the SkyTyper script
 deleteCreate   - Run 'delete' command and then 'create' command with the
                  SkyTyper script
 start          - Start all startable instances
 stop           - Stop all stoppable instances
 print          - Print SkyTyper script stacks to standard out
                  as CloudFormation JSON
 export         - Export SkyTyper script stacks to file(s)
                  as CloudFormation JSON
 summary        - Print summary of SkyTyper script stacks to standard out in
                  Markdown format
 checkmodel     - Check SkyTyper stack models with internal validation
 validate       - Run validate using CloudFormation API
 clean          - Remove SkyTyper notification topic and queue
 printconfig    - Print rendered SkyTyper configuration to console
'''
    }

    static void printInitHelp() {
        println "TODO: Add init documentation"
    }

    static void printCreateHelp() {
        println "TODO: Add create documentation"
    }

    static void printUpdateHelp() {
        println "TODO: Add update documentation"
    }

    static void printDeleteHelp() {
        println "TODO: Add delete documentation"
    }

    static void printCreateDeleteHelp() {
        println "TODO: Add createDelete documentation"
    }

    static void printStartHelp() {
        println "TODO: Add start documentation"
    }

    static void printStopHelp() {
        println "TODO: Add stop documentation"
    }

    static void printPrintHelp() {
        println "TODO: Add print documentation"
    }

    static void printExportHelp() {
        println "TODO: Add export documentation"
    }

    static void printSummaryHelp() {
        println "TODO: Add summary documentation"
    }

    static void printCheckModelsHelp() {
        println "TODO: Add check models documentation"
    }

    static void printValidateHelp() {
        println "TODO: Add validate documentation"
    }

    static void printCleanHelp() {
        println "TODO: Add clean documentation"
    }

    static void printShowConfigHelp() {
        println "TODO: Add show config documentation"
    }


}