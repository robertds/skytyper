/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper

import skytyper.model.Template
import skytyper.model.TemplateBuilder
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Slf4j
import org.hibernate.validator.constraints.NotEmpty

import javax.validation.constraints.Pattern

@Slf4j
abstract class SkyTyperStack {
    HashMap<String, String> buildProperties
    public Template template
    @Pattern(regexp = /[a-zA-Z][-a-zA-Z0-9]*/)
    @NotEmpty
    String stackName = ''
    boolean iamCapability = false

    abstract stack()

    SkyTyperStack() {
        template = new Template()
    }

    SkyTyperStack build() {
        stack()
        this
    }

    void template(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = TemplateBuilder) Closure closure) {
        def newTemplate = new TemplateBuilder()
        closure.delegate = newTemplate
        closure()
        template = newTemplate
    }

    @Override
    String toString() {
        new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(template)
    }

    Collection<String> getCapabilities() {
        def capabilities = new ArrayList<String>()
        if (iamCapability) capabilities << "CAPABILITY_IAM"
        capabilities
    }

}
