/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.validation

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


class ValidCIDRBlockValidator implements ConstraintValidator<ValidCIDRBlock, Object> {
    static final String CIDR_BLOCK_REGEX = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\\/(\d|[1-2]\d|3[0-2]))$/

    public void initialize(ValidCIDRBlock constraintAnnotation) { }

    boolean isValid(def object, ConstraintValidatorContext constraintContext) {
        if (object == null)
            return true

        if (object instanceof String)
            return (object ==~ CIDR_BLOCK_REGEX)
        else
            return ((object?.Ref instanceof String) || (object['Fn::FindInMap'] != null))
        //TODO Double-check the CIDR block validator
    }
}
