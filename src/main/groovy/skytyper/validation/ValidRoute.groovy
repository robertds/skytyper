/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.validation

import skytyper.model.ec2.Route

import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import java.lang.annotation.Documented
import java.lang.annotation.Retention
import java.lang.annotation.Target

import static java.lang.annotation.ElementType.TYPE
import static java.lang.annotation.RetentionPolicy.RUNTIME


@Target( [ TYPE ] )
@Retention(RUNTIME)
@Constraint(validatedBy = ValidRouteValidator.class)
@Documented
public @interface ValidRoute {
    String message() default "must provide exactly one of the following: GatewayID, InstanceID, or NetworkInterfaceID";

    Class<?>[] groups() default []

    Class<? extends Payload>[] payload() default []
}

class ValidRouteValidator implements ConstraintValidator<ValidRoute, Route> {
    public void initialize(ValidRoute constraintAnnotation) { }

    boolean isValid(Route route, ConstraintValidatorContext constraintContext) {
        (route.properties.gatewayID.asBoolean() ^ route.properties.instanceID.asBoolean()
                ^ route.properties.networkInterfaceID.asBoolean())
    }
}