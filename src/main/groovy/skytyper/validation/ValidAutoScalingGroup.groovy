/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.validation

import skytyper.model.autoscaling.AutoScalingGroup

import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import java.lang.annotation.Documented
import java.lang.annotation.Retention
import java.lang.annotation.Target

import static java.lang.annotation.ElementType.TYPE
import static java.lang.annotation.RetentionPolicy.RUNTIME


@Target( [ TYPE ] )
@Retention(RUNTIME)
@Constraint(validatedBy = ValidAutoScalingGroupValidator.class)
@Documented
public @interface ValidAutoScalingGroup {
    String message() default "must provide exactly one of the following: LaunchConfigurationName or InstanceID";

    Class<?>[] groups() default []

    Class<? extends Payload>[] payload() default []
}

class ValidAutoScalingGroupValidator implements ConstraintValidator<ValidAutoScalingGroup, AutoScalingGroup> {
    public void initialize(ValidAutoScalingGroup constraintAnnotation) { }

    boolean isValid(AutoScalingGroup autoScalingGroup, ConstraintValidatorContext constraintContext) {
        (autoScalingGroup.properties.launchConfigurationName.asBoolean() ^ autoScalingGroup.properties.instanceID.asBoolean())
    }
}