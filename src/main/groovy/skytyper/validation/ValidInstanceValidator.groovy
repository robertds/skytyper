/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.validation

import skytyper.model.ec2.EC2Instance

import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class ValidInstanceValidator implements ConstraintValidator<ValidInstance, EC2Instance> {
    public void initialize(ValidInstance constraintAnnotation) { }

    boolean isValid(EC2Instance instance, ConstraintValidatorContext constraintContext) {
        !((instance.properties.securityGroupIDs?.size() > 0) && (instance.properties.securityGroups?.size() > 0))
    }
}