/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.script

import skytyper.SkyTyperStack
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * This is a helper class to parse the config object and generate a SkyTyperScript instance for the Runner
 */
class ConfigParser {
    protected static final Logger log = LoggerFactory.getLogger(this.class)

    static Config getConfig(String configFilename) {
        def fileConfig = ConfigFactory.parseFile(new File(configFilename))
        if (fileConfig.empty) {
            log.error("Default configuration file '$configFilename' not found.\nCheck to ensure file exists or specify alternate filename with --config option.\n")
            System.exit(1)
        }
        def conf = ConfigFactory.load(fileConfig)
        conf.checkValid(conf)
        conf
    }

    static SkyTyperScript generate(Config config, String labelOption) {
        def label = (labelOption) ? labelOption : getDefaultLabel(config)
        def configSteps = config.getConfigList("skytyper.labels.${label}.steps")
        String configPrefix = config.getString("skytyper.labels.${label}.prefix")
        def parameterObject = config.hasPath("skytyper.labels.${label}.parameters") ? config.getObject("skytyper.labels.${label}.parameters") : null

        def script = new SkyTyperScript() {
            //Ignore, since we build the script below
            @Override
            def script() { return null }
        }

        for (int i = 0; i < configSteps.size(); i++) {
            if (configSteps[i].hasPath("shell")) {
                def when = (configSteps[i].hasPath("when")) ? configSteps[i].getStringList("when") : null

                def shellStep = new ShellStep()
                shellStep.with {
                    name = configSteps[i].getString("name")
                    command = configSteps[i].getString("shell")
                    runOn = when?.collect { ScriptAction.fromValue(it) }
                }
                script.steps += shellStep

            } else if (configSteps[i].hasPath("skytyper")) {
                def skytyperFilename = configSteps[i].getString("skytyper")
                def stackStep = new StackStep(configSteps[i].getString("name"), configPrefix)
                stackStep.stack = loadStack(skytyperFilename).build()
                script.steps += stackStep

            } else if (configSteps[i].hasPath("groovy")) {
                def when = (configSteps[i].hasPath("when")) ? configSteps[i].getStringList("when") : null
                def groovyFilename = configSteps[i].getString("groovy")
                def groovyStep = new GroovyStep()
                groovyStep.with {
                    name = configSteps[i].getString("name")
                    filename = groovyFilename
                    runOn = when?.collect { ScriptAction.fromValue(it) }
                }
                script.steps += groovyStep

            } else {
                log.error("SkyTyper configuration file contains an invalid step. Steps should contain one or more of 'skytyper', 'groovy', 'shell'.")
                System.exit(1)
            }
        }

        script.runtimeParameters = parameterObject?.unwrapped()
        script
    }

    static SkyTyperStack loadStack(String filename) {
        def targetScript = null
        try {
            targetScript = new GroovyScriptEngine('.').loadScriptByName(filename)
        } catch (Exception e) {
            log.error "Script resource [$filename] not found. ${e.message}"
            System.exit(1)
        }
        def target = targetScript.newInstance()

        if (target instanceof SkyTyperStack) {
            target
        } else {
            log.error "\u001B[31mNo usable stacks in file ${filename}\u001B[0m"
            null
        }
    }

    static String getDefaultLabel(Config config) {
        if (config.hasPath("skytyper.defaults.label"))
            config.getString("skytyper.defaults.label")
        else {
            log.error "\u001B[31mInvalid configuration: No 'skytyper.defaults.label' found.\u001B[0m"
            System.exit(1)
            "Error"
        }
    }
}
