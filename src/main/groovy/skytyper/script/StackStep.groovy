/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.script

import skytyper.SkyTyperStack
import skytyper.model.Region
import skytyper.model.Template
import skytyper.util.AWSServiceHandler
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Slf4j
import org.joda.time.DateTime
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import org.joda.time.format.PeriodFormatter
import org.joda.time.format.PeriodFormatterBuilder

import javax.validation.Validation
import javax.validation.ValidatorFactory

import static org.fusesource.jansi.Ansi.ansi

@Slf4j
class StackStep extends ScriptStep {
    AWSServiceHandler awsServiceHandler
    static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern('yyyy-MM-dd HH:mm:ss z')
    static final PeriodFormatter PERIOD_FORMATTER = new PeriodFormatterBuilder()
            .appendDays()
            .appendSuffix(" day", " days")
            .appendSeparatorIfFieldsBefore(", ")
            .appendHours()
            .appendSuffix(" hour", " hours")
            .appendSeparatorIfFieldsBefore(", ")
            .appendMinutes()
            .appendSuffix(" minute", " minutes")
            .appendSeparatorIfFieldsBefore(" and ")
            .appendSecondsWithOptionalMillis()
            .appendSuffix(" seconds").toFormatter()
    Region region
    SkyTyperStack stack
    private String stackStepPrefix = ''
    boolean startable = true
    boolean stoppable = true
    boolean updateable = true
    boolean deleteable = true


    StackStep(String name, String prefix) {
        this.name = name
        stackStepPrefix = prefix
    }

    Map<String, Object> run(ScriptAction action, Map<String, Object> runtimeParameters) {
        def parameters = new HashMap<String, Object>()
        switch (action) {
            case ScriptAction.CHECK_MODEL:
                checkModel()
                break
            case ScriptAction.VALIDATE:
                checkModel()
                validateStack()
                break
            case ScriptAction.PRINT:
                printStack()
                break
            case ScriptAction.EXPORT:
                exportStack()
                break
            case ScriptAction.SUMMARY:
                printStackSummary()
                break
            case ScriptAction.CREATE:
                checkModel()
                parameters = createStack(runtimeParameters)
                break
            case ScriptAction.DELETE:
                checkModel()
                deleteStack()
                break
            case ScriptAction.UPDATE:
                checkModel()
                parameters = updateStack(runtimeParameters)
                break
            case ScriptAction.CLEAN:
                cleanup()
                break
            case ScriptAction.START:
                start()
                break
            case ScriptAction.STOP:
                stop()
                break
            default:
                log.error "Action '${action}' not implemented."

        }
        parameters
    }

    void printStack() {
        println "Print stack: ${fullStackName(stack)}\n"
        println new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(stack.template) + "\n"
    }

    void checkModel() {
        log.info "Checking SkyTyper stack model..."
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
        def validator = factory.getValidator()

        def constraintViolations = validator.validate(stack)
        constraintViolations.each {
            log.warn (ansi().render(" @|bold,red \u2717|@ @|bold ${fullStackName(stack)}|@ internal validation @|bold,red failed|@:\n      ${it.propertyPath}: ${it.message}").toString())
        }
        if (!constraintViolations)
            log.info (ansi().render(" @|bold,green \u2714|@ @|bold ${fullStackName(stack)}|@ internal validation passed").toString())
    }

    void validateStack() {
        try {
            awsServiceHandler = new AWSServiceHandler()
            log.info "Validating CloudFormation stack..."
            awsServiceHandler.validateStack(fullStackName(stack), stack.template)
        } catch (Exception e) {
            log.error(e.message)
        }
    }

    void printStackSummary() {
        String thinSeparator = '\n------------------------------------------------------------------\n'

        println "# Stack Summary\n_Generated at ${DATE_FORMATTER.print(new DateTime())}_"
        int parameterCount = 0
        int mappingCount = 0
        int resourceCount = 0
        int outputCount = 0

        println thinSeparator
        println "## Stack: ${stack.stackName}"
        println "  * Does${(!stack.iamCapability)? " not" : ''} have IAM Capability"
        Template template = stack.template
        println "  * Contains a SkyTyper Template"
        println "  * Description: ${template.templateDescription}\n"
        if (template.parameters) {
            println "### Parameters"
            template.parameters.each {
                println "  * Name: __${it.key}__\n    Description: __${it.value.description}__"
            }
            parameterCount = template.parameters.size()
        } else {
            println "### No parameters"
        }
        println ''

        if (template.mappings) {
            println "### Mappings"
            template.mappings.keySet().each {
                println "  * Key: __${it}__  Value: __${template.mappings.get(it)}__"
            }
            mappingCount = template.mappings.size()
        } else {
            println "### No mappings"
        }
        println ''

        if (template.resources) {
            println "### Resources"
            template.resources.each {
                println "  * __${it.key}__ of type __${it.value.type}__"
            }
            resourceCount = template.resources.size()
        } else {
            println "### No resources"
        }
        println ''

        if (template.outputs) {
            println "### Outputs"
            template.outputs.each {
                println "  * Name: __${it.key}__  Value: __${it.value.value}__"
            }
            outputCount = template.outputs.size()
        } else {
            println "### No outputs"
        }
        println ''

        println "### Stack totals"
        println "  * $parameterCount parameter${(parameterCount != 1) ? "s" : ""}"
        println "  * $mappingCount mapping${(mappingCount != 1) ? "s" : ""}"
        println "  * $resourceCount resource${(resourceCount != 1) ? "s" : ""}"
        println "  * $outputCount output${(outputCount != 1) ? "s" : ""}"

        println thinSeparator
    }

    void exportStack() {
        log.info("Exporting stack \'${stack.stackName}\' to file \'${fullStackName(stack)}.json\'")
        new File("${stackStepPrefix}${stack.stackName}.json").withWriter { out ->
            out.write(stack.template.toString())
        }
    }

    Map<String, Object> createStack(Map<String, Object> runtimeParameters) {
        if (!runtimeParameters) runtimeParameters = [:]
        try {
            awsServiceHandler = new AWSServiceHandler()

            log.debug "Creating stack..."
            DateTime startTime = DateTime.now()
            runtimeParameters.putAll(awsServiceHandler.createStack(fullStackName(stack), stack.template, runtimeParameters, stack.getCapabilities()))
            DateTime endTime = DateTime.now()
            Period period = new Period(startTime,endTime)
            log.info(ansi().render("Processed @|bold ${stackStepPrefix+stack.stackName}|@ in ${PERIOD_FORMATTER.print(period)}").toString())
        } catch (Exception e) {
            log.error(e.message)
        }
        runtimeParameters
    }

    Map<String, Object> updateStack(Map<String, Object> runtimeParameters) {
        if (!runtimeParameters) runtimeParameters = [:]
        if (updateable) {
            try {
                awsServiceHandler = new AWSServiceHandler()
                log.debug "Updating stack..."
                DateTime startTime = DateTime.now()
                runtimeParameters.putAll(awsServiceHandler.updateStack(fullStackName(stack), stack.template, runtimeParameters, stack.getCapabilities()))
                DateTime endTime = DateTime.now()
                Period period = new Period(startTime,endTime)
                log.info(ansi().render("Processed @|bold ${fullStackName(stack)}|@ in ${PERIOD_FORMATTER.print(period)}").toString())
            } catch (Exception e) {
                log.error(e.message)
            }
        } else {
            log.warn "${fullStackName(stack)} is configured as 'not updateable', skipping stack"
        }
        runtimeParameters
    }

    void deleteStack() {
        if (deleteable) {
            try {
                awsServiceHandler = new AWSServiceHandler()
                log.debug "Deleting stack..."
                awsServiceHandler.deleteStack(fullStackName(stack))
            } catch (Exception e) {
                log.error(e.message)
            }
        } else {
            log.warn "${fullStackName(stack)} is configured as 'not deletable', skipping stack"
        }
    }

    void start() {
        if (startable) {
            try {
                awsServiceHandler = new AWSServiceHandler()
                log.info "Starting stack instance(s)..."
                awsServiceHandler.deleteStack(fullStackName(stack))
            } catch (Exception e) {
                log.error(e.message)
            }
        } else {
            log.warn "${fullStackName(stack)} is configured as 'not startable', skipping stack"
        }


    }

    void stop() {
        if (stoppable) {
            try {
                awsServiceHandler = new AWSServiceHandler()
                log.info "Stopping stack instance(s)..."
                awsServiceHandler.deleteStack(fullStackName(stack))
            } catch (Exception e) {
                log.error(e.message)
            }
        } else {
            log.warn "${fullStackName(stack)} is configured as 'not stoppable', skipping stack"
        }
    }

    void cleanup() {
        try {
            awsServiceHandler = new AWSServiceHandler()
            awsServiceHandler.tearDownNotificationQueue()
        } catch (Exception e) {
            log.error(e.message)
        }
    }

    String fullStackName(SkyTyperStack stack) {
        stackStepPrefix+stack.stackName
    }


}
