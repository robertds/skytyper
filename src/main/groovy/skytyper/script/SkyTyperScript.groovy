/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.script

import groovy.util.logging.Slf4j

@Slf4j
abstract class SkyTyperScript {

    ScriptStep[] steps = []
    Map<String, Object> runtimeParameters

    abstract script()

    void run(ScriptAction action) {
        switch (action) {
            case ScriptAction.DELETE_CREATE:
                doAction(ScriptAction.DELETE)
                doAction(ScriptAction.CREATE)
                break
            default:
                doAction(action)
        }
    }

    StackStep stackStep(String name, String prefix, @DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = StackStep) Closure closure) {
        def newStackStep = new StackStep(name, prefix)
        closure.delegate = newStackStep
        closure()
        steps += newStackStep
        newStackStep
    }


    private def doAction(ScriptAction action) {
        if (action == ScriptAction.DELETE) {
            steps.reverse().each {
                runtimeParameters = it.run(action, runtimeParameters)
            }
        } else {
            steps.each {
                runtimeParameters = it.run(action, runtimeParameters)
            }
        }
    }
}

