/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.script


enum ScriptAction {
    CREATE("create"),
    UPDATE("update"),
    DELETE("delete"),
    DELETE_CREATE("deletecreate"),
    PRINT("print"),
    EXPORT("export"),
    SUMMARY("summary"),
    CHECK_MODEL("checkmodel"),
    VALIDATE("validate"),
    CLEAN("clean"),
    START("start"),
    STOP("stop")

    private String name

    String value() {
        name
    }

    ScriptAction(String s) { name = s }

    static ScriptAction fromValue(String typeString) {
        for (ScriptAction t : values()) {
            if (t.value() == typeString.toLowerCase()) {
                return t;
            }
        }
        throw new IllegalArgumentException("Invalid ScriptAction value: " + typeString)
    }
}
