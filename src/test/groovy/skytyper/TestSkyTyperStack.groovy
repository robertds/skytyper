/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper

import skytyper.model.Tag
import skytyper.model.autoscaling.AutoScalingTag
import skytyper.model.autoscaling.NotificationConfigurationProperty
import skytyper.model.ec2.InstanceType
import skytyper.model.ec2.SecurityGroupProtocol


class TestSkyTyperStack extends SkyTyperStack {
    def stack() {
        stackName = "TestSkyTyperStack"
        template {
            templateDescription = 'SkyTyper example template'

            // Parameters
            def keyNameParameter = parameter.string ("KeyName") {
                description = "The KeyName for the workspace instance"
            }

            // Mappings
            mapping ([ 'AWSRegionAMIs': [ 'us-east-1': [ 32: 'ami-6411e20d', 64: 'ami-7a11e213' ] ] ])

            // Resources
            def workspaceSecurityGroup = resource.ec2.securityGroup ("WorkspaceSecurityGroup") {
                properties {
                    description = "This is a Security Group for a SkyTyper Example demo Workspace"
                    ingress {
                        cidrBlock = '172.31.0.0/18'
                        from = 80
                        to = 80
                        protocol = SecurityGroupProtocol.TCP
                    }
                }
            }

            def instance = resource.ec2.instance ("SkyTyperExampleInstance") {
                properties {
                    imageID = 'ami-05355a6c'
                    securityGroups = [ ref(workspaceSecurityGroup), "Default" ]
                    instanceType = InstanceType.T1_MICRO
                    keyName = ref(keyNameParameter)
                    tags = [ new Tag(key: 'MyTagName', value: 'MyTagValue'),
                            new Tag(key: '2ndTagName', value: '2ndTagValue')]
                }
            }

            resource.ec2.elasticIP ("SkyTyperExampleEIP") {
                properties {
                    instanceId = ref(instance)
                }
            }


            resource.autoscaling.autoScalingGroup("DemoASG") {
                properties {
                    minSize = 1
                    maxSize = 1
                    tags = [ new AutoScalingTag(key: 'MyASKey', value: 'MyASValue', propagateAtLaunch: false) ]
                    notificationConfiguration = new NotificationConfigurationProperty()
                }
                updatePolicy {
                    autoScalingRollingUpdate {
                        minInstancesInService = 1
                    }
                }
            }

            // Outputs
            output ("SkyTyperExampleSecurityGroup") {
                description = "My example security group output"
                value = ref(workspaceSecurityGroup)
            }

        }
    }
}