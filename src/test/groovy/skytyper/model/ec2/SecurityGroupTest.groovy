/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.ec2

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

class SecurityGroupTest extends GroovyTestCase {
    protected final Logger log = LoggerFactory.getLogger(this.class)
    static Validator validator
    static EC2SecurityGroup sampleData

    @Override
    protected void setUp() throws Exception {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
        validator = factory.getValidator()

        sampleData = new EC2SecurityGroup()
        sampleData.properties {
            name = "TestSecurityGroup"
            description = "Test Security Group Description"
            ingressList << new EC2SecurityGroupRule(cidrBlock: '0.0.0.0/0', from: 80, to: 80)
        }
    }

    void testOutput() {
        def mapper = new ObjectMapper()
        def output = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(sampleData)
        assertEquals(sampleOutput, output)
    }

    void testValidation() {
        def invalidResource = new EC2SecurityGroup()

        def constraintViolations = validator.validate(invalidResource)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }

        assertEquals(1, constraintViolations.size())
    }

    static final String sampleOutput =
'''{
  "Type" : "AWS::EC2::SecurityGroup",
  "Properties" : {
    "GroupDescription" : "Test Security Group Description",
    "SecurityGroupIngress" : [ {
      "CidrIp" : "0.0.0.0/0",
      "FromPort" : 80,
      "ToPort" : 80
    } ]
  }
}'''

}
