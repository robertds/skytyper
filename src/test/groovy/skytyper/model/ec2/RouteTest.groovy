/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.ec2

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

class RouteTest extends GroovyTestCase {
    static final Logger log = LoggerFactory.getLogger(this.class)
    Validator validator
    Route sampleResource


    @Override
    protected void setUp() throws Exception {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
        validator = factory.getValidator()
        sampleResource = new Route()
        sampleResource.with {
            name = "Test Route"
            properties.with {
                destinationCIDRBlock = '10.0.0.1/16'
                gatewayID = 'igw-12345678'
                routeTableID = 'routetableid'
            }
        }
    }

    void testJsonOutput() {
        def output = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(sampleResource)
        assertEquals(SAMPLE_CLOUDFORMATION_JSON, output)
    }

    void testJsonInput() {
        def resourceInput = new ObjectMapper().readValue(SAMPLE_CLOUDFORMATION_JSON, Route.class)
        assertEquals("AWS::EC2::Route", resourceInput.type)
        assertEquals("routetableid", resourceInput.properties.routeTableID)
        assertEquals("10.0.0.1/16", resourceInput.properties.destinationCIDRBlock)
        assertEquals("igw-12345678", resourceInput.properties.gatewayID)
    }


    void testDefaultValidation() {
        def constraintViolations = validator.validate(new Route())
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }

        assertEquals(4, constraintViolations.size())
    }

    void testSampleValidation() {
        def constraintViolations = validator.validate(sampleResource)
        assertEquals(0, constraintViolations.size())
    }

    void testSampleValidationWithConstraintCheck() {
        sampleResource.properties.networkInterfaceID = 'networkInterfaceID'
        def constraintViolations = validator.validate(sampleResource)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }
        assertEquals(1, constraintViolations.size())
    }


    static final String SAMPLE_CLOUDFORMATION_JSON =
'''{
  "Type" : "AWS::EC2::Route",
  "Properties" : {
    "DestinationCidrBlock" : "10.0.0.1/16",
    "GatewayId" : "igw-12345678",
    "RouteTableId" : "routetableid"
  }
}'''

}
