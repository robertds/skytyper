/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.ec2

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

class InstanceTest extends GroovyTestCase {
    protected final Logger log = LoggerFactory.getLogger(this.class)
    static Validator validator
    static EC2Instance sampleResource

    @Override
    protected void setUp() throws Exception {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
        validator = factory.getValidator()
        sampleResource = new EC2Instance()
        sampleResource.with {
            name = "MyInstance"
            properties.with {
                availabilityZone = "us-east-1a"
                blockDeviceMappings += new BlockDeviceMappingProperty(deviceName: 'aDeviceName',
                        ebsBlockDevice: new EBSBlockDeviceProperty(deleteOnTermination: true,
                                iops: 100,
                                snapshotID: 'aSnapshotID'),
                        virtualName: 'aVirtualName')
                imageID = 'ami-12345678'
                instanceType = InstanceType.C1_MEDIUM
            }
        }

    }

    void testJsonOutput() {
        def output = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(sampleResource)
        assertEquals(sampleInstanceCloudFormationJson, output)
    }

    void testJsonInput() {
        def instanceInput = new ObjectMapper().readValue(sampleInstanceCloudFormationJson, EC2Instance.class)
        assertEquals("AWS::EC2::Instance", instanceInput.type)
        //assertEquals("aSnapshotID", instanceInput.blockDeviceMappings[0].ebsBlockDevice.snapshotID)
        assertEquals("ami-12345678", instanceInput.properties.imageID)
        assertEquals("c1.medium", instanceInput.properties.instanceType)
    }

    void testClassLevelValidation() {
        def instance = sampleResource
        instance.properties.securityGroupIDs = [ 'sg-12345678', 'sg-87654321' ]
        instance.properties.securityGroups = null

        def constraintViolations = validator.validate(instance)
        constraintViolations.each {
            log.error "Unexpected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }
        assertEquals(0, constraintViolations.size())

        instance.properties.securityGroupIDs = null
        instance.properties.securityGroups = [ 'sg-12345678', 'sg-87654321' ]

        constraintViolations = validator.validate(instance)
        constraintViolations.each {
            log.error "Unexpected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }
        assertEquals(0, constraintViolations.size())

        instance.properties.securityGroupIDs = null
        instance.properties.securityGroups = null

        constraintViolations = validator.validate(instance)
        constraintViolations.each {
            log.error "Unexpected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }
        assertEquals(0, constraintViolations.size())

        instance.properties.securityGroupIDs = [ 'sg-12345678', 'sg-87654321' ]
        instance.properties.securityGroups = [ 'sg-12345678', 'sg-87654321' ]
        constraintViolations = validator.validate(instance)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }
        assertEquals(1, constraintViolations.size())

    }

    void testValidationErrors() {
        def invalidResource = new EC2Instance()

        def constraintViolations = validator.validate(invalidResource)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }

        assertEquals(2, constraintViolations.size())
    }

    static final String sampleInstanceCloudFormationJson =
'''{
  "Type" : "AWS::EC2::Instance",
  "Properties" : {
    "AvailabilityZone" : "us-east-1a",
    "BlockDeviceMappings" : [ {
      "DeviceName" : "aDeviceName",
      "Ebs" : {
        "DeleteOnTermination" : true,
        "Iops" : 100,
        "SnapshotId" : "aSnapshotID"
      },
      "NoDevice" : false,
      "VirtualName" : "aVirtualName"
    } ],
    "DisableApiTermination" : false,
    "EbsOptimized" : false,
    "ImageId" : "ami-12345678",
    "InstanceType" : "c1.medium",
    "Monitoring" : false,
    "SourceDestCheck" : false
  }
}'''

}
