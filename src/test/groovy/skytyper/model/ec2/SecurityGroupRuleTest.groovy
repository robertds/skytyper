/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.ec2

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

//FIXME This test is not written correctly
class SecurityGroupRuleTest extends GroovyTestCase {
    protected final Logger log = LoggerFactory.getLogger(this.class)
    static Validator validator

    @Override
    protected void setUp() throws Exception {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
        validator = factory.getValidator()
    }

    void testClassLevelValidation() {
        def resource = new EC2SecurityGroupRule()
        resource.to = 80
        resource.from = 80
        resource.protocol = SecurityGroupProtocol.TCP

        resource.cidrBlock = '10.0.1.1/24'
        resource.sourceSecurityGroupId = ''
        resource.sourceSecurityGroupName = null

        def constraintViolations = validator.validate(resource)
        constraintViolations.each {
            log.error "Unexpected constraint violation found: ${it.rootBeanClass.name} ${it.propertyPath} ${it.message}"
        }
        assertEquals(0, constraintViolations.size())

        resource.cidrBlock = null
        resource.sourceSecurityGroupId = 'sg-12345678'
        resource.sourceSecurityGroupName = null

        constraintViolations = validator.validate(resource)
        constraintViolations.each {
            log.error "Unexpected constraint violation found: ${it.rootBeanClass.name} ${it.propertyPath} ${it.message}"
        }
        assertEquals(0, constraintViolations.size())

        resource.cidrBlock = null
        resource.sourceSecurityGroupId = null
        resource.sourceSecurityGroupName = 'MySecurityGroup'

        constraintViolations = validator.validate(resource)
        constraintViolations.each {
            log.error "Unexpected constraint violation found: ${it.rootBeanClass.name} ${it.propertyPath} ${it.message}"
        }
        assertEquals(0, constraintViolations.size())

        resource.cidrBlock = '10.0.0.1/24'
        resource.sourceSecurityGroupId = 'sg-12345678'
        resource.sourceSecurityGroupName = 'MySecurityGroup'
        constraintViolations = validator.validate(resource)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name} ${it.propertyPath} ${it.message}"
        }
        assertEquals(1, constraintViolations.size())

        resource.cidrBlock = null
        resource.sourceSecurityGroupId = 'sg-12345678'
        resource.sourceSecurityGroupName = 'MySecurityGroup'
        constraintViolations = validator.validate(resource)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name} ${it.propertyPath} ${it.message}"
        }
        assertEquals(1, constraintViolations.size())

        resource.cidrBlock = '10.0.0.1/24'
        resource.sourceSecurityGroupId = null
        resource.sourceSecurityGroupName = 'MySecurityGroup'
        constraintViolations = validator.validate(resource)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name} ${it.propertyPath} ${it.message}"
        }
        assertEquals(1, constraintViolations.size())

        resource.cidrBlock = '10.0.0.1/24'
        resource.sourceSecurityGroupId = 'sg-12345678'
        resource.sourceSecurityGroupName = null
        constraintViolations = validator.validate(resource)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name} ${it.propertyPath} ${it.message}"
        }
        assertEquals(1, constraintViolations.size())

    }


    void testCIDRBlockValidation() {
        def resource = new EC2SecurityGroupRule()
        resource.to = 80
        resource.from = 80
        resource.protocol = SecurityGroupProtocol.TCP

        //Test for valid CIDR regex
        resource.cidrBlock = "172.31.0.0/18"
        def constraintViolations = validator.validate(resource)
        constraintViolations.each {
            log.error "Unexpected constraint violation found: ${it.rootBeanClass.name} ${it.propertyPath} ${it.message}"
        }
        assertEquals(0, constraintViolations.size())

        //Test for invalid CIDR regex
        resource.cidrBlock = "999.999.999.999/99"
        constraintViolations = validator.validate(resource)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name} ${it.propertyPath} ${it.message}"
        }
        assertEquals(1, constraintViolations.size())

        //TODO: Review these tests to see if they are still needed
//        //Test for a Ref(erence) to another Resource
//        def parameterResource = new StringParameter(name: 'MyParam', defaultValue: '172.31.0.0/18')
//        resource.cidrBlock = (SkyTyperStack.ref(parameterResource))
//        constraintViolations = validator.validate(resource)
//        constraintViolations.each {
//            log.error "Unexpected constraint violation found: ${it.rootBeanClass.name} ${it.propertyPath} ${it.message}"
//        }
//        assertEquals(0, constraintViolations.size())
//
//        //Test for a FindInMap
//        resource.cidrBlock = (SkyTyperStack.findInMap("MyCidrBlock"))
//        constraintViolations = validator.validate(resource)
//        constraintViolations.each {
//            log.error "Unexpected constraint violation found: ${it.rootBeanClass.name} ${it.propertyPath} ${it.message}"
//        }
//        assertEquals(0, constraintViolations.size())
    }

    void testValidation() {
        def invalidResource = new EC2SecurityGroupRule()

        def constraintViolations = validator.validate(invalidResource)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name} ${it.propertyPath} ${it.message}"
        }

        assertEquals(5, constraintViolations.size())
    }
}
