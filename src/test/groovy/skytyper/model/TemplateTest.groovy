/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model

import skytyper.SkyTyperStack
import skytyper.model.ec2.BlockDeviceMappingProperty
import skytyper.model.ec2.EBSBlockDeviceProperty
import skytyper.model.ec2.EC2Instance
import skytyper.model.ec2.InstanceType
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

class TemplateTest extends GroovyTestCase {
    protected final Logger log = LoggerFactory.getLogger(this.class)
    static Validator validator
    static SkyTyperStack sampleStack
    static Template sampleTemplate
//    static Template sampleTemplate2

    @Override
    protected void setUp() throws Exception {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
        validator = factory.getValidator()

        sampleTemplate = new Template()
        sampleTemplate.templateDescription = "A sample template for unit testing."
        def myParam = new StringParameter(name: "MyParam", noEcho: true, defaultValue: "MyValue")
        sampleTemplate.add myParam
        def sampleResource = new EC2Instance()
        sampleResource.with {
            name = "MyInstance"
            properties.with {
                availabilityZone = "us-east-1a"
                blockDeviceMappings += new BlockDeviceMappingProperty(deviceName: 'aDeviceName',
                        ebsBlockDevice: new EBSBlockDeviceProperty(deleteOnTermination: true,
                                iops: 100,
                                snapshotID: 'aSnapshotID'),
                        virtualName: 'aVirtualName')
                imageID = 'ami-12345678'
                setKeyName(ref(myParam))
                instanceType = InstanceType.C1_MEDIUM
            }
        }
        sampleTemplate.add sampleResource
        sampleTemplate.add new Output(name: "MyParamOutput", value: ['Ref' : 'MyParam'])

        sampleStack = new SkyTyperStack() {
            @Override
            def stack() {
                return sampleTemplate
            }
        }
        sampleStack.iamCapability = true
        sampleStack.stackName = "TestStack"
    }

    void testJsonInput() {
        def mapper = new ObjectMapper()
        Template templateInput = mapper.readValue(sampleTemplateOutput, Template.class)
        assertEquals("A sample template for unit testing.", templateInput.templateDescription)
        assertEquals(true, templateInput.parameters["MyParam"].noEcho)
        assertEquals("MyValue", templateInput.parameters["MyParam"].defaultValue)
        assertEquals([ 'Ref' : 'MyParam'], templateInput.outputs["MyParamOutput"].value)
    }


    void testJsonOutput() {
        def mapper = new ObjectMapper()
        def output = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(sampleTemplate)
        assertEquals(sampleTemplateOutput, output)
    }

    void testRoundTripWithRef() {
        def mapper = new ObjectMapper()
        Template templateInput = mapper.readValue(sampleTemplateOutput, Template.class)
        def output = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(templateInput)
        assertEquals(sampleTemplateOutput, output)
    }

    void testRoundTripWithString() {
        def mapper = new ObjectMapper()
        Template templateInput = mapper.readValue(sampleTemplateOutputWithString, Template.class)
        def output = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(templateInput)
        assertEquals(sampleTemplateOutputWithString, output)
    }


    void testValidation() {
        def stack = new SkyTyperStack() {
            @Override
            def stack() {
                return null
            }
        }

        def constraintViolations = validator.validate(stack)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }

        assertEquals(2, constraintViolations.size())
    }

    static final String sampleTemplateOutput =
'''{
  "AWSTemplateFormatVersion" : "2010-09-09",
  "Description" : "A sample template for unit testing.",
  "Parameters" : {
    "MyParam" : {
      "Type" : "String",
      "Default" : "MyValue",
      "NoEcho" : true
    }
  },
  "Resources" : {
    "MyInstance" : {
      "Type" : "AWS::EC2::Instance",
      "Properties" : {
        "AvailabilityZone" : "us-east-1a",
        "BlockDeviceMappings" : [ {
          "DeviceName" : "aDeviceName",
          "Ebs" : {
            "DeleteOnTermination" : true,
            "Iops" : 100,
            "SnapshotId" : "aSnapshotID"
          },
          "NoDevice" : false,
          "VirtualName" : "aVirtualName"
        } ],
        "DisableApiTermination" : false,
        "EbsOptimized" : false,
        "ImageId" : "ami-12345678",
        "InstanceType" : "c1.medium",
        "KeyName" : {
          "Ref" : "MyParam"
        },
        "Monitoring" : false,
        "SourceDestCheck" : false
      }
    }
  },
  "Outputs" : {
    "MyParamOutput" : {
      "Value" : {
        "Ref" : "MyParam"
      }
    }
  }
}'''


    static final String sampleTemplateOutputWithString =
            '''{
  "AWSTemplateFormatVersion" : "2010-09-09",
  "Description" : "A sample template for unit testing.",
  "Parameters" : {
    "MyParam" : {
      "Type" : "String",
      "Default" : "MyValue",
      "NoEcho" : true
    }
  },
  "Resources" : {
    "MyInstance" : {
      "Type" : "AWS::EC2::Instance",
      "Properties" : {
        "AvailabilityZone" : "us-east-1a",
        "BlockDeviceMappings" : [ {
          "DeviceName" : "aDeviceName",
          "Ebs" : {
            "DeleteOnTermination" : true,
            "Iops" : 100,
            "SnapshotId" : "aSnapshotID"
          },
          "NoDevice" : false,
          "VirtualName" : "aVirtualName"
        } ],
        "DisableApiTermination" : false,
        "EbsOptimized" : false,
        "ImageId" : "ami-12345678",
        "InstanceType" : "c1.medium",
        "KeyName" : "TestKey",
        "Monitoring" : false,
        "SourceDestCheck" : false
      }
    }
  },
  "Outputs" : {
    "MyParamOutput" : {
      "Value" : {
        "Ref" : "MyParam"
      }
    }
  }
}'''

}
