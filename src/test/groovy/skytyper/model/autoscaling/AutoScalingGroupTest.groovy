/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.autoscaling

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory


class AutoScalingGroupTest extends GroovyTestCase {
    static final Logger log = LoggerFactory.getLogger(this.class)
    Validator validator
    AutoScalingGroup sampleResource


    @Override
    protected void setUp() throws Exception {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
        validator = factory.getValidator()
        sampleResource = new AutoScalingGroup("TestAutoScalingGroup")
        sampleResource.with {
            properties {
                availabilityZones = ['us-east-1']
                launchConfigurationName = 'TestLaunchConfig'
                maxSize = 2
                minSize = 1
                cooldown = ref("cooldown")
            }
            updatePolicy {
                autoScalingRollingUpdate {
                    maxBatchSize = 3
                    minInstancesInService = 4
                    pauseTime = 'PT30M30S'
                }
            }
        }
    }

    void testJsonOutput() {
        def output = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(sampleResource)
        assertEquals(SAMPLE_CLOUDFORMATION_JSON, output)
    }

    void testJsonInput() {
        def resourceInput = new ObjectMapper().readValue(SAMPLE_CLOUDFORMATION_JSON, AutoScalingGroup.class)
        assertEquals("AWS::AutoScaling::AutoScalingGroup", resourceInput.type)
        assertEquals('us-east-1', resourceInput.properties.availabilityZones[0])
        assertEquals(1, resourceInput.properties.minSize)
        assertEquals(2, resourceInput.properties.maxSize)
        assertEquals(3, resourceInput.updatePolicy.autoScalingRollingUpdate.maxBatchSize)
        assertEquals(4, resourceInput.updatePolicy.autoScalingRollingUpdate.minInstancesInService)
    }


    void testDefaultValidation() {
        def constraintViolations = validator.validate(new AutoScalingGroup())
        constraintViolations.each {
            log.info "Expected constraint violation found: ${it.rootBeanClass.name} ${it.propertyPath} ${it.message}"
        }

        assertEquals(5, constraintViolations.size())
    }

    void testSampleValidation() {
        def constraintViolations = validator.validate(sampleResource)
        assertEquals(0, constraintViolations.size())
    }

    void testSampleValidationWithConstraintCheck() {
        sampleResource.properties.instanceID = 'i-12345678'
        def constraintViolations = validator.validate(sampleResource)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name} ${it.propertyPath} ${it.message}"
        }
        assertEquals(1, constraintViolations.size())
    }

    static final String SAMPLE_CLOUDFORMATION_JSON =
'''{
  "Type" : "AWS::AutoScaling::AutoScalingGroup",
  "Properties" : {
    "AvailabilityZones" : [ "us-east-1" ],
    "Cooldown" : {
      "Ref" : "cooldown"
    },
    "LaunchConfigurationName" : "TestLaunchConfig",
    "MaxSize" : 2,
    "MinSize" : 1
  },
  "UpdatePolicy" : {
    "AutoScalingRollingUpdate" : {
      "MaxBatchSize" : 3,
      "MinInstancesInService" : 4,
      "PauseTime" : "PT30M30S"
    }
  }
}'''


}
