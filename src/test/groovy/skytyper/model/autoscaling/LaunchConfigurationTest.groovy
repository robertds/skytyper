/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.autoscaling

import skytyper.model.ec2.InstanceType
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

class LaunchConfigurationTest extends GroovyTestCase {
    static final Logger log = LoggerFactory.getLogger(this.class)
    Validator validator
    LaunchConfiguration sampleResource


    @Override
    protected void setUp() throws Exception {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
        validator = factory.getValidator()
        sampleResource = new LaunchConfiguration("Test Resource")
        sampleResource.with {
            properties {
                associatePublicIPAddress = true
                def ebsBlockDevice = new AutoScalingEBSBlockDeviceProperty()
                ebsBlockDevice.deleteOnTermination = true
                ebsBlockDevice.iops = 500
                ebsBlockDevice.snapshotID = 'snap-12345678'

                def blockDevice = new AutoScalingBlockDeviceMappingProperty()
                blockDevice.deviceName = "TestDevice"
                blockDevice.noDevice = false
                blockDevice.ebsBlockDevice = ebsBlockDevice
                blockDeviceMappings = [ blockDevice ]
                ebsOptimized = true
                imageID = "ami-12345678"
                instanceMonitoring = true
                instanceType = InstanceType.M1_SMALL
            }
        }
    }

    void testJsonOutput() {
        def output = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(sampleResource)
        assertEquals(SAMPLE_CLOUDFORMATION_JSON, output)
    }

    void testJsonInput() {
        def resourceInput = new ObjectMapper().readValue(SAMPLE_CLOUDFORMATION_JSON, LaunchConfiguration.class)
        assertEquals("AWS::AutoScaling::LaunchConfiguration", resourceInput.type)
        assertEquals("m1.small", resourceInput.properties.instanceType as String)
        assertEquals(true, resourceInput.properties.instanceMonitoring)
        assertEquals("snap-12345678", resourceInput.properties.blockDeviceMappings[0].ebsBlockDevice.snapshotID)
    }


    void testDefaultValidation() {
        def constraintViolations = validator.validate(new LaunchConfiguration())
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }

        assertEquals(3, constraintViolations.size())
    }

    void testSampleValidation() {
        def constraintViolations = validator.validate(sampleResource)
        assertEquals(0, constraintViolations.size())
    }



    static final String SAMPLE_CLOUDFORMATION_JSON =
            '''{
  "Type" : "AWS::AutoScaling::LaunchConfiguration",
  "Properties" : {
    "AssociatePublicIpAddress" : true,
    "BlockDeviceMappings" : [ {
      "DeviceName" : "TestDevice",
      "Ebs" : {
        "DeleteOnTermination" : true,
        "Iops" : 500,
        "SnapshotId" : "snap-12345678"
      },
      "NoDevice" : false
    } ],
    "EbsOptimized" : true,
    "ImageId" : "ami-12345678",
    "InstanceMonitoring" : true,
    "InstanceType" : "m1.small"
  }
}'''

}
