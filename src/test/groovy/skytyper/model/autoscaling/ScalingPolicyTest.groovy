/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.autoscaling

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory


class ScalingPolicyTest extends GroovyTestCase {

    static final Logger log = LoggerFactory.getLogger(this.class)
    Validator validator
    ScalingPolicy sampleResource


    @Override
    protected void setUp() throws Exception {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
        validator = factory.getValidator()
        sampleResource = new ScalingPolicy("TestResource")
        sampleResource.with {
            properties {
                adjustmentType = ScalingPolicyAdjustmentType.EXACT_CAPACITY
                autoScalingGroupName = "TestAutoScalingGroup"
                cooldown = 60
                scalingAdjustment = 10
            }
        }
    }

    void testJsonOutput() {
        def output = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(sampleResource)
        assertEquals(SAMPLE_CLOUDFORMATION_JSON, output)
    }

    void testJsonInput() {
        def resourceInput = new ObjectMapper().readValue(SAMPLE_CLOUDFORMATION_JSON, ScalingPolicy.class)
        assertEquals("AWS::AutoScaling::ScalingPolicy", resourceInput.type)
        assertEquals(ScalingPolicyAdjustmentType.EXACT_CAPACITY, resourceInput.properties.adjustmentType)
        assertEquals(60, resourceInput.properties.cooldown)
        assertEquals(10, resourceInput.properties.scalingAdjustment)
    }


    void testDefaultValidation() {
        def constraintViolations = validator.validate(new ScalingPolicy())
        constraintViolations.each {
            log.info "Expected constraint violation found: ${it.rootBeanClass.name} ${it.propertyPath} ${it.message}"
        }

        assertEquals(4, constraintViolations.size())
    }

    void testSampleValidation() {
        def constraintViolations = validator.validate(sampleResource)
        assertEquals(0, constraintViolations.size())
    }

    static final String SAMPLE_CLOUDFORMATION_JSON =
'''{
  "Type" : "AWS::AutoScaling::ScalingPolicy",
  "Properties" : {
    "AdjustmentType" : "ExactCapacity",
    "AutoScalingGroupName" : "TestAutoScalingGroup",
    "Cooldown" : 60,
    "ScalingAdjustment" : 10
  }
}'''

}
