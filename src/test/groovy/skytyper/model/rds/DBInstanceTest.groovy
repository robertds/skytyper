/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.rds

import groovy.json.JsonBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

class DBInstanceTest extends GroovyTestCase {
    protected final Logger log = LoggerFactory.getLogger(this.class)
    static Validator validator
    static DBInstance sampleResource

    @Override
    protected void setUp() throws Exception {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
        validator = factory.getValidator()
        sampleResource = new DBInstance()
        sampleResource.with {
            name = "TestInstance"
            availabilityZone = "us-east-1a"
            dbInstanceClass = DBInstanceClass.DB_M1_SMALL
            port = 5000
            masterUsername = 'username'
            masterUserPassword = 'password'
            allocatedStorage = 1
            engine = DBEngine.MYSQL
        }

    }

    void testOutput() {
        assertEquals(sampleResourceOutput, new JsonBuilder(sampleResource.data()).toPrettyString())
    }

    void testClassLevelValidation() {
        def dbInstance = sampleResource
        dbInstance.vpcSecurityGroups = [ 'sg-12345678', 'sg-87654321' ]
        dbInstance.dbSecurityGroups = null

        def constraintViolations = validator.validate(dbInstance)
        constraintViolations.each {
            log.error "Unexpected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }
        assertEquals(0, constraintViolations.size())

        dbInstance.vpcSecurityGroups = null
        dbInstance.dbSecurityGroups = [ 'sg-12345678', 'sg-87654321' ]

        constraintViolations = validator.validate(dbInstance)
        constraintViolations.each {
            log.error "Unexpected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }
        assertEquals(0, constraintViolations.size())

        dbInstance.vpcSecurityGroups = [ 'sg-12345678', 'sg-87654321' ]
        dbInstance.dbSecurityGroups = [ 'sg-12345678', 'sg-87654321' ]
        constraintViolations = validator.validate(dbInstance)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }
        assertEquals(1, constraintViolations.size())

    }

    void testValidationErrors() {
        def invalidResource = new DBInstance()

        def constraintViolations = validator.validate(invalidResource)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }
        assertEquals(7, constraintViolations.size())
    }

    static final String sampleResourceOutput =
'''{
    "Type": "AWS::RDS::DBInstance",
    "Properties": {
        "AllocatedStorage": 1,
        "AutoMinorVersionUpgrade": true,
        "AvailabilityZone": "us-east-1a",
        "DBInstanceClass": "db.m1.small",
        "Engine": "mysql",
        "MasterUsername": "username",
        "MasterUserPassword": "password",
        "Port": 5000
    }
}'''

}
