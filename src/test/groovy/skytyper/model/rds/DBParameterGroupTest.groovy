/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.rds

import groovy.json.JsonBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

class DBParameterGroupTest extends GroovyTestCase {
    protected final Logger log = LoggerFactory.getLogger(this.class)
    static Validator validator
    static DBParameterGroup sampleResource

    @Override
    protected void setUp() throws Exception {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
        validator = factory.getValidator()
        sampleResource = new DBParameterGroup()
        sampleResource.with {
            name = "TestDBParameterGroup"
            description = "Test DBParameterGroup Description"
            parameters += [ "key1": "value1", "key2": "value2" ]
        }

    }

    void testOutput() {
        assertEquals(sampleInstanceOutput, new JsonBuilder(sampleResource.data()).toPrettyString())
    }


    void testValidationErrors() {
        def invalidResource = new DBParameterGroup()

        def constraintViolations = validator.validate(invalidResource)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }

        assertEquals(4, constraintViolations.size())
    }

    static final String sampleInstanceOutput =
'''{
    "Type": "AWS::RDS::DBParameterGroup",
    "Properties": {
        "Description": "Test DBParameterGroup Description",
        "Parameters": {
            "key1": "value1",
            "key2": "value2"
        }
    }
}'''

}
