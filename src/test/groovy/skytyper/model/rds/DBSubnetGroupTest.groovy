/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.model.rds

import groovy.json.JsonBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

class DBSubnetGroupTest extends GroovyTestCase {
    protected final Logger log = LoggerFactory.getLogger(this.class)
    static Validator validator
    static DBSubnetGroup sampleResource

    @Override
    protected void setUp() throws Exception {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
        validator = factory.getValidator()
        sampleResource = new DBSubnetGroup()
        sampleResource.with {
            name = "TestDBSubnetGroup"
            description = "Test DBSubnetGroup Description"
            subnetIDs +=  [ "subnet-12345678" ]
        }

    }

    void testOutput() {
        assertEquals(sampleInstanceOutput, new JsonBuilder(sampleResource.data()).toPrettyString())
    }


    void testValidationErrors() {
        def invalidResource = new DBSubnetGroup()

        def constraintViolations = validator.validate(invalidResource)
        constraintViolations.each {
            log.debug "Expected constraint violation found: ${it.rootBeanClass.name}.${it.propertyPath} ${it.message}"
        }

        assertEquals(2, constraintViolations.size())
    }

    static final String sampleInstanceOutput =
'''{
    "Type": "AWS::RDS::DBSubnetGroup",
    "Properties": {
        "DBSubnetGroupDescription": "Test DBSubnetGroup Description",
        "SubnetIds": [
            "subnet-12345678"
        ]
    }
}'''

}
