/*
 * Copyright (c) 2014 Robert Schanafelt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package skytyper.script

import com.typesafe.config.Config
import com.typesafe.config.ConfigException
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ConfigParserTest extends GroovyTestCase {
    protected final Logger log = LoggerFactory.getLogger(this.class)
    static final String TEST_CONF_FILENAME = "src/test/resources/test.conf"
    static final String TEST_LABEL = "test"
    private Config testConfig

    @Override
    protected void setUp() throws Exception {
        testConfig = ConfigParser.getConfig(TEST_CONF_FILENAME)
//        log.info(testConfig.root().render())
    }

    void testConfigLoading() {
//        log.info(testConfig.root().render())
        assertEquals("test",testConfig.getString("skytyper.defaults.label"))
    }

    void testConfigSteps() {
        def steps = testConfig.getConfigList("skytyper.labels.test.steps")

        for (int i = 0; i < steps.size(); i++) {
            println steps[i].getString("name")
            try {
                def shell = steps[i].getString("shell")
                def when = steps[i].getStringList("when")
                if (shell) println("Found shell script: "+shell)
                when.each { println "When: " + it }
            } catch (ConfigException e) {}

            try {
                def skytyper = steps[i].getString("skytyper")
                if (skytyper) println("Found skytyper stack: "+skytyper)
            } catch (ConfigException e) {}

        }

        assertEquals(3, steps.size())
    }

    void testConfigGeneration() {
        def script = ConfigParser.generate(ConfigParser.getConfig(TEST_CONF_FILENAME), TEST_LABEL)

        assertEquals(3,script.steps.size())

    }


}
