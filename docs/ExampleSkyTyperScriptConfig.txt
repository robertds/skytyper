example skytyper.conf script runner
# Notes:
# Inventory? 
# It would be cool to pull inventory from AWS (e.g. InstanceId of instance named "MyInstance")
# treat stacks as immutable? allow for stack model overrides?

# Process: Config Properties + Stack output => Run next stack (with Config settings)

## Example below

defaults.mode = dev
defaults.region = us-east-1

defaults.properties {
	myparam1 = hello
	myparam2 = 1234	
}


defaults.base {
  steps.1 {
	  purpose: load template for MyProduction stack from existing CloudFormation stack
	  command.stack {
	    source.parseFromCloudFormation: "MyProductionStack"
	  }
	  on: export, generate, // or "all"
  }

	steps.2 {
	  purpose: get outputs from MyOtherStack
	  command.outputsFromCloudFormation: "MyOtherStack"
	  onError: stop // or 'ignore'
  }

	steps.3 {
      purpose: run some useful command
      command.shell: ./localScript.sh
    }

	steps.4 {
  	  purpose = the main stack
      onError: stop
      condition: always // ??
	  command.stack {
        source.skytypertemplate = MyStack.groovy
        action: print // action should really only be from the CLI
        stoppable: true
        updatable: false
        outputs: ignore // or gather??
        ensure: created // probably not this approach
        region: us-east-1 // rare override
        iam-capability: true // rare override
      }
    }	

	steps.5 {
	  purpose: run skytyper script to do special things
	  skytyperscript: skytyperScriptFile('MyScript.groovy')
	    action: run
    }
}

# Declare the modes to be used for this project
# (e.g. base, dev, test, stage, prod, etc.)
modes {
  dev = ${defaults.base} {
    prefix = dev-
    failfast = true
    disable-rollback: true

    steps.1.stack.action = ignore
    steps.2.stack.action = create
    steps.3.stack.action = always
  }
  
  test = ${defaults.base} {
    prefix = test-
    failfast = false
     
    file('settings.conf')

    step.1.stack.action = ignore
  }
}


