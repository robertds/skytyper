import skytyper.SkyTyperStack
import skytyper.model.ec2.SecurityGroupProtocol

class Demo1Stack extends SkyTyperStack {
    def stack() {
        stackName = "Demo1Stack"
        template {
            templateDescription = 'Demo1 Security Group stack'

            resource.ec2.elasticIP("MyElasticIP") {}

            def demo1SecurityGroup = resource.ec2.securityGroup('Demo1SecurityGroup') {
                properties {
                    description = 'This is a Security Group for a SkyTyper demo'
                    ingress {
                        cidrBlock = '0.0.0.0/0'
                        from = 80
                        to = 80
                        protocol = SecurityGroupProtocol.TCP
                    }
                    ingress {
                        cidrBlock = '0.0.0.0/0'
                        from = 22
                        to = 22
                        protocol = SecurityGroupProtocol.TCP
                    }
                }
            }

            output('Demo1SecurityGroup') {
                value = ref(demo1SecurityGroup)
            }
        }
    }
}
