import skytyper.script.ScriptAction
import skytyper.script.SkyTyperScript
import skytyper.script.StackStep

class DemoScript extends SkyTyperScript {
    def script() {
        steps += new StackStep(new Demo1Stack().build())
        steps += new StackStep(new Demo2Stack().build())
    }

    static main(args) {
        def demo = new DemoScript()
        demo.action = ScriptAction.VALIDATE
        demo.configFilename = "Demo.conf"
        demo.run()
    }
}
