import skytyper.script.ScriptAction
import skytyper.script.SkyTyperScript

class SkyTyperScriptExample extends SkyTyperScript {

    def script() {

        // Demonstrate overriding a stack property
        def myFluentStack = new SkyTyperFluentExample(buildProperties: [ '32-ami' : 'ami-6411e20d' ]).build()
        myFluentStack.template.outputs.get("SkyTyperExampleSecurityGroup").value = "OVERRIDE!"

        stackStep("Name","ExamplePrefix-") {
            stack = myFluentStack
            description = "This is a description for MyFluentStack"
        }

    }

    static main(args) {
        def example = new SkyTyperScriptExample()
        example.script()
        example.run(ScriptAction.VALIDATE)
    }
}
