
import skytyper.SkyTyperStack
import skytyper.model.*
import skytyper.model.autoscaling.*
import skytyper.model.ec2.*


class SkyTyperFluentExample extends SkyTyperStack {
    def stack() {
        stackName = "SkyTyperFluentExampleStack"
        template {
            templateDescription = 'SkyTyper example template'

            // Parameters
            def keyNameParameter = parameter.string ("KeyName") {
                description = "The KeyName for the workspace instance"
            }

            // Mappings
            mapping ([ 'AWSRegionAMIs': [ 'us-east-1': [ 32: buildProperties['32-ami'], 64: 'ami-7a11e213' ] ] ])

            // Resources
            def workspaceSecurityGroup = resource.ec2.securityGroup ("WorkspaceSecurityGroup") {
                properties {
                    description = "This is a Security Group for a SkyTyper Example demo Workspace"
                    ingress {
                        cidrBlock = '172.31.0.0/18'
                        from = 80
                        to = keyNameParameter
                        protocol = SecurityGroupProtocol.TCP
                    }
                }
            }

            def instance = resource.ec2.instance ("SkyTyperExampleInstance") {
                properties {
                    imageID = 'ami-05355a6c'
                    securityGroups = [ ref(workspaceSecurityGroup), "Default" ]
                    instanceType = InstanceType.T1_MICRO
                    keyName = ref(keyNameParameter)
                    tags = [ new Tag(key: 'MyTagName', value: 'MyTagValue'),
                            new Tag(key: 'AWSAccount', value: refAWSAccountId())]
                }
            }

            resource.ec2.elasticIP ("SkyTyperExampleEIP") {
                properties {
                    instanceId = ref(instance)
                }
            }

            resource.autoscaling.autoScalingGroup("DemoASG") {
                properties {
                    minSize = 1
                    setMaxSize(instance)
                    tags = [ new AutoScalingTag(key: 'MyASKey', value: 'MyASValue', propagateAtLaunch: false) ]
                    notificationConfiguration = new NotificationConfigurationProperty()
                }
                updatePolicy {
                    autoScalingRollingUpdate {
                        minInstancesInService = 1
                    }
                }
            }

            // Outputs
            output ("SkyTyperExampleSecurityGroup") {
                description = "My example security group output"
                value = ref(workspaceSecurityGroup)
            }

        }
    }

    static main(args) {
        print new SkyTyperFluentExample(buildProperties : [ '32-ami' : 'ami-6411e20d' ]).build().template
    }
}
