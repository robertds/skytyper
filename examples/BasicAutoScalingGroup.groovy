import skytyper.SkyTyperStack
import skytyper.model.autoscaling.AutoScalingTag
import skytyper.model.autoscaling.NotificationConfigurationProperty
import skytyper.model.autoscaling.NotificationType

class BasicAutoScalingGroup extends SkyTyperStack {
    def stack() {
        stackName = "BasicAutoScalingGroup"
        template {
            templateDescription = "A simple AutoScalingGroup with CloudFormation"

            //Parameters
            def minSizeParameter = parameter.number("MinSize") {
                description = "Minimum ASG size"
                defaultValue = 3
            }

            def maxSizeParameter = parameter.number("MaxSize") {
                description = "Maximum ASG size"
                defaultValue = 15
            }

            resource.autoscaling.autoScalingGroup("DemoASG") {
                properties {
                    minSize = ref(minSizeParameter)
                    maxSize = ref(maxSizeParameter)
                    tags = [ new AutoScalingTag(key: 'Name', value: 'DemoASG', propagateAtLaunch: false) ]
                    notificationConfiguration = new NotificationConfigurationProperty()
                    notificationConfiguration.notificationTypes = NotificationType.allTypes()
                }
                updatePolicy {
                    autoScalingRollingUpdate {
                        minInstancesInService = 1
                    }
                }
            }


        }

    }

    static void main(args) {
        println(new BasicAutoScalingGroup().build().toString())
    }
}
