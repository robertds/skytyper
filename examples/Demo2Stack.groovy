import skytyper.SkyTyperStack
import skytyper.model.ec2.EC2Instance
import skytyper.model.ec2.InstanceType
import skytyper.model.Output
import skytyper.model.StringParameter
import skytyper.model.Tag

class Demo2Stack extends SkyTyperStack {
    def stack() {
        stackName = "Demo2Stack"

        template.with {
            templateDescription = 'Demo 2 instance stack'

            def sgParameter = new StringParameter(
                    name: 'Demo1SecurityGroup',
                    description: 'The security group from the demo 1 stack')
            add sgParameter


            def instance = new EC2Instance(name: 'DemoInstance')
            instance.properties.with {
                    imageID = 'ami-05355a6c'
                    securityGroups = [ ref(sgParameter) ]
                    instanceType = InstanceType.C1_MEDIUM
                    keyName = 'demokey'
                    userData = new File('examples/simple.sh').text
                    tags = [ new Tag(key: 'FirstTagName', value: 'FirstTagValue'),
                            new Tag(key: '2ndTagName', value: '2ndTagValue')]
            }
            add instance

            add new Output(name: "MyInstance", value: ref(instance))
        }
    }
}
