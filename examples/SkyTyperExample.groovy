import skytyper.SkyTyperStack
import skytyper.model.*
import skytyper.model.ec2.*

class SkyTyperExample extends SkyTyperStack {
    def stack() {
        stackName = "SkyTyperExampleStack"
        template {
            description = 'SkyTyper example template'

            // Parameter
            def keynameParameter = new StringParameter(
                    name: 'KeyName',
                    description: 'The KeyName for the workspace instance')
            add keynameParameter


            // Mapping
            add ([ 'AWSRegionAMIs': [ 'us-east-1': [ 32: buildProperties['32-ami'], 64: 'ami-7a11e213' ] ] ])


            // Resources
            def workspaceSecurityGroup = new EC2SecurityGroup(name: 'WorkspaceSecurityGroup')
            workspaceSecurityGroup.properties.description = 'This is a Security Group for a SkyTyper Example demo Workspace'

            workspaceSecurityGroup.properties.ingress << new EC2SecurityGroupRule(cidrBlock: '172.31.0.0/18',
                    from: 80,
                    to: 80,
                    protocol: SecurityGroupProtocol.TCP)
            add workspaceSecurityGroup

            def instance = new EC2Instance(name: 'SkyTyperExampleInstance')
            instance.properties.with {
                imageID = 'ami-05355a6c'
                securityGroups = [ ref(workspaceSecurityGroup), "Default" ]
                instanceType = InstanceType.T1_MICRO
                keyName = ref(keynameParameter)
                tags = [ new Tag(key: 'MyTagName', value: 'MyTagValue'),
                        new Tag(key: '2ndTagName', value: '2ndTagValue')]
            }
            add instance

            def eip = new EIP(name: 'SkyTyperExampleEIP')
            eip.properties.instanceId = ref(instance)
            add eip

            // Output
            add new Output(name: 'SkyTyperExampleSecurityGroup',
                    value: ref(workspaceSecurityGroup),
                    description: "My example security group")

        }
    }

    static main(args) {
        print new SkyTyperExample(buildProperties : [ '32-ami' : 'ami-6411e20d' ]).build()
    }
}
