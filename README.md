# AWS SkyTyper

## > > > Notice: Alpha release! < < < ##

This is a proof-of-concept release that is in early development and only has coverage for about 40% of CloudFormation functionality. Class names and package organization will almost certainly be refactored before getting to a usable release.

## Introduction

CloudFormation is a tool to design and manage multiple AWS resources in a way that can be automated and versioned like any other application code. 

SkyTyper is a tool that works alongside CloudFormation to enhance developer productivity, reduce errors, shorten wait times, and further automate the use of multiple stacks.

## Key Features

* Productivity in IDEs with type checking of stack resources
* More programmer-friendly syntax than JSON
* Define and run multiple stacks at a time
* Idempotency - can re-run a list of stacks to speed development
* Output-->Parameter hand off between stacks
* Validate stacks with greater accuracy than CloudFormation alone
* Composable stacks with the import feature
* Detailed logging and metrics for stack operations
* Summary and statistics for a list of stacks
* Offline stack validation
* 100% compatibility with CloudFormation means no lock-in to SkyTyper
* Built-in export of CloudFormation templates
* Potential future productivity features:
	* Blueprints of common patterns (e.g. VPC & subnet creation)
	* Stop/start select resources in a stack to reduce utilization

## Getting Started ##

* Download the SkyTyper distribution
* Setup your favorite IDE to use the SkyTyper library
* Write SkyTyper scripts and go!

### Important Note about "prefixes" ###

SkyTyper uses prefixes (as noted in the example below) to uniquely identify a set of CloudFormation stacks to execute. In order to maintain developer productivity you should use the same prefix for the "groups" of stacks that you want to manage together.

In other words, you can launch and test separate groups of CloudFormation templates by using a unique prefix for each group. For example, this allows you to create a development group of stacks by using a prefix of say "Dev-", and a group of stacks for staging by using a prefix of "Stage-", or your own stack with "Username-dev-", and so on.

### Command Line Example ###

    skytyper create MyPrefix- Example.groovy

## IDE setup ##

Unzip the SkyTyper distribution. You should have a /bin folder with platform-dependent shell scripts, and a /lib folder containing the SkyTyper library jar and its dependencies.

Add the SkyTyper jar and dependency jars to a library in your IDE.

There are two main ways to run SkyTyper scripts:

1) Run the skytyper automation tool using the bin/skytyper shell script you installed from the zip file. (Be sure to set your AWS_SECRET_KEY and AWS_ACCESS_KEY environment variables.)

2) Set up your IDE to run the Groovy script runner by setting up a "Run configuration" as a Java Application.

  - Point the **Main class** to **skytyper.util.Runner**
  - Set the program arguments to the desired action (e.g. "validate MyPrefix- SkyTyperExample.groovy")
  - Place an awscredentials.properties file in your CLASSPATH with your AWS credentials (or set System/Environment Variables in the Run configuration)

Bonus tip: If you add a static main method to your SkyTyper script as noted below (and in the **SkyTyperExample.groovy** file) you can quickly and easily print out the full CloudFormation templates without being online and without running any command line tools.

For example, for a script named *SkyTyperExample* add this:

    static main(args) {
        print new SkyTyperExample()
    }

Then run this:

    groovy --classpath '<YOUR FULL PATH TO>/skytyper/lib/*' SkyTyperExample.groovy

### IDE setup for SkyTyper library development ###

**IntelliJ**

To generate your IntelliJ project workspace, in the project root directory, run:

    gradle idea


**Eclipse**

To generate your Eclipse project workspace, in the project root directory, run:

    gradle eclipse

### Building the Command Line Tool and Library for distribution ###

To generate and package the command line tool interface, run the following gradle task:

    gradle installApp

This will place *nix and windows batch files in **$PROJECT_DIR/build/install/skytyper/bin** and the required library jars in **$PROJECT_DIR/build/install/skytyper/lib**. You can create a link to this executable or place the directory on your system $PATH to make the script runner available to your OS.


## SkyTyper Script Example ##

See **SkyTyperExample.groovy** in the project root directory.

## Advanced use cases ##

Loading and overriding stack values.

For example,

        def stack = new SkyTyperFluentExample(buildProperties: [ '32-ami' : 'ami-6411e20d' ]).build()
        stack.template.outputs.get("SkyTyperExampleSecurityGroup").value = "OVERRIDE!"
